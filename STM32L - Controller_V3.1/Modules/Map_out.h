#ifndef		_MAP_OUT_H__
#define		_MAP_OUT_H__


//自控通道基础配置描述符	//总计长度 18个字节
typedef __packed struct
{
	unsigned char ChannelID;		//通道编号
	unsigned char ChannelType;		//设备类型
	unsigned char ChannelParam[8];	//ChannelParam[0-3]：控制端口,ChannelParam[4-7]：反馈端口
	unsigned char ChannelReverse[8];	//保留
	
} MAP_OUT_ITEM;




unsigned int SetChannelConfig(unsigned int index, MAP_OUT_ITEM *item);

MAP_OUT_ITEM *GetChannelConfigInfo( uint16 index,unsigned char * ChannelConfig );






#endif
