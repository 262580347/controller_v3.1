#ifndef _CONTROL_H_
#define	_CONTROL_H_

#define		CTR_RALAY_TYPE1_4_8		GPIOB

#define		CTR_R1_PIN		GPIO_Pin_12
#define		CTR_R2_PIN		GPIO_Pin_13
#define		CTR_R3_PIN		GPIO_Pin_14
#define		CTR_R4_PIN		GPIO_Pin_15


#define		CTR_R1_ON()			GPIO_SetBits(CTR_RALAY_TYPE1_4_8, CTR_R1_PIN);
#define		CTR_R1_OFF()		GPIO_ResetBits(CTR_RALAY_TYPE1_4_8, CTR_R1_PIN);

#define		CTR_R2_ON()			GPIO_SetBits(CTR_RALAY_TYPE1_4_8, CTR_R2_PIN);
#define		CTR_R2_OFF()		GPIO_ResetBits(CTR_RALAY_TYPE1_4_8, CTR_R2_PIN);

#define		CTR_R3_ON()			GPIO_SetBits(CTR_RALAY_TYPE1_4_8, CTR_R3_PIN);
#define		CTR_R3_OFF()		GPIO_ResetBits(CTR_RALAY_TYPE1_4_8, CTR_R3_PIN);

#define		CTR_R4_ON()			GPIO_SetBits(CTR_RALAY_TYPE1_4_8, CTR_R4_PIN);
#define		CTR_R4_OFF()		GPIO_ResetBits(CTR_RALAY_TYPE1_4_8, CTR_R4_PIN);

#define		CTR_VCC_TYPE		GPIOB
#define		CTR_VCC_PIN			GPIO_Pin_1

#define		CTR_VCC_ON()		GPIO_SetBits(CTR_VCC_TYPE, CTR_VCC_PIN);
#define		CTR_VCC_OFF()		GPIO_ResetBits(CTR_VCC_TYPE, CTR_VCC_PIN);

#define OUTPUT_CHANNEL_COUNT	(4)

#include "Map_out.h"

//通道自控方案描述符结构体
typedef __packed struct
{	
	unsigned char ChannelID;		//输出通道
	unsigned char valid;			//通道配置区内容是否有效

	unsigned char afc_type;			//0--本地、1--远程控制
	
	unsigned int time_acc;		//累计时间
	
	unsigned char control_action;	//需要进行的控制动作
	
	unsigned char start_action;		//开始动作
	unsigned char end_action;		//结束动作
	unsigned int duration;		//控制持续时间（分钟）
	
	unsigned char last_action;		//最后控制状态
	
	unsigned char need_to_check;	//是否需要检测反馈
	unsigned char check_result;		//状态反馈检测结果
	
	MAP_OUT_ITEM ChannelConfig;	//输出通道相关的参数信息(方便检索用的)	
	
} CHANNEL_INFO;

////通道状态描述符
typedef struct
{
	unsigned short ch_type;	  	//类型
	unsigned char afc_type;		//控制方式：0--本地,1--远程,需要设置默认为远程,本地控制功能暂时不用
	unsigned char is_manual;	//是否手动控制
	unsigned char mark;			//数据标识符
	void *content;		//消息数据
} CHANNEL_STATUS;

void SetControlTimeFlag(unsigned char isTrue);

void Control_Process(void);

unsigned int set_channel_config(unsigned int index, MAP_OUT_ITEM *item);

unsigned int output_channel_init(unsigned char index);

MAP_OUT_ITEM * get_channel_config_item( unsigned int index );

unsigned int output_manual_action(unsigned char ChannelID, unsigned char *content);

unsigned int output_get_status(unsigned char ChannelID, CHANNEL_STATUS *ch_status);

unsigned char GetOutputChannelCount(void);

void Init_Relay_Port(void);

void Init_Relay_Power(void);

void Deinit_Relay_Power(void);

void Deinit_Relay_Port(void);

unsigned int CheckChannelState( void );

extern unsigned char g_ArriveReportingTime;

void SetReCMDFlag(unsigned char isTrue);

unsigned char GetReCMDFlag(void);

unsigned char GetLeveloutputStatus(void);

#endif
