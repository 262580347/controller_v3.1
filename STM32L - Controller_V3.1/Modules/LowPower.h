#ifndef  _LOW_POWER_H_
#define  _LOW_POWER_H_

#define		MEASUREMENT_INTERVAL  1 
#define		STOP_TIME		10

	
void EnterStopMode(unsigned char Second);
void EnterStopMode_SIM(unsigned char Second);	
void EnterStopMode_WIFI(unsigned char Second);
void EnterStandbyMode(void);
void EnterSleepMode(void);

void EXTI_Aux_Config(void);
void EXTI_Aux_Reset(void);
void EXTI17_Reset(void);
void ResetAwakeCount(void);
void StartGetBatVol(void);
void Exit_Awake_Process(unsigned short g_nMain10ms);

void LowPower_Process(void);
	
void StopModeProcess(void);
	
void SetStopModeFlag(unsigned char isTrue);

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime);

unsigned char GetStopModeFlag(void);
#endif
