#include "485Sensor.h"
#include "main.h"

#include <math.h>
//	485传感器指令格式：
//	地址	功能码		寄存器地址		寄存器个数		CRC-L		CRC-H		嘉创
//	0x02	0x03		0x00 0x2A		0x00 0x01		0xA5		0xF1

//	地址	功能码		寄存器地址		寄存器个数		CRC-L		CRC-H		新普惠
//	0x01	0x03		0x00 0x00		0x00 0x01		0x84		0x0A

#define		ERROR_STATE_NORMAL			0x00
#define		ERROR_STATE_OVERTIME		0x01
#define		ERROR_STATE_SENSORERROR		0x02
#define		ERROR_STATE_CRCERROR		0x03
#define		ERROR_STATE_OVERDATA		0x04

#define		ERROR_485VALUE		0XEEEEEEEE
#define		ERROR_SENSOR_DATA	0XFFFF
#define		ERROR_SOIL_M		655.35
#define		SENSOR485_BAND_RATE	9600
#define 	CRC_SEED   0xFFFF   
#define 	POLY16 		0x1021  

#define		CMD_FUNCTION		0x06	//修改地址
#define		CMD_INQRIRE			0x09	//查询地址

#define		SAMPLING_INTERVAL	12

static  CHANNEL_DATA		SensorChannelData;

static unsigned char s_InputChannelCount = 0;

unsigned char g_485SensorGetFlag = FALSE;

unsigned short g_ReportDataWaitingTime = 0;	//上报间隔(S)



void EXTI_Rainfall_Reset(void);

unsigned char GetInputChannelCount(void)
{
	return s_InputChannelCount;
}

float GetSensorData(unsigned char SensorNum)
{
	return SensorChannelData.ChannelData[SensorNum];
}


unsigned char Uart_Send_StartMeasure(unsigned char SensorAddr)
{
	static unsigned char data[8];
	unsigned short CRCVal = 0;
	u16 nMain10ms = 0, i;
	u8 Err = 0;
	
	data[0] = SensorAddr;
	data[1] = 0x03;
	data[2] = 0x25;
	data[3] = 0x00;	
	data[4] = 0x00;
	data[5] = 0x01;
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;	
	
	delay_ms(100);
	Uart_Send_Data(SENSOR485_COM, data, 8);
	
	nMain10ms = GetSystem10msCount();
	while(CalculateTime(GetSystem10msCount(), nMain10ms) < 30)
	{
		if(g_Uart2RxFlag == TRUE)
		{
			if((g_USART2_RX_CNT) > 5 && (SensorAddr == g_USART2_RX_BUF[0]))
			{
				Err = 1;
			}
			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				u1_printf("%02X ",g_USART2_RX_BUF[i]);
			}
			u1_printf("\r\n");
			Clear_Uart2Buff();
			break;
		}	
	}
//	TimerID_StartMeasure = SetTimer(300, TimerWaitMeasureAck);
	return Err;
}



void Uart_Send_485CMD(MAP_IN_ITEM *item)
{
	static unsigned char data[8] = {0x02, 0x03, 0x00, 0x00, 0x00, 0x01, 0xA5, 0XF1};
	unsigned short CRCVal = 0;
	
	data[0] = item->in_param[2];
	if((item->data_id == CO2_ID && item->in_param[4] == 0 && item->in_param[3] == 0) || item->in_param[5])	// 04功能码获取数据
	{
		data[1] = 0x04;
	}
	else
	{
		data[1] = 0x03;
	}
	
	data[2] = item->in_param[3];
	data[3] = item->in_param[4];	
	if(item->data_id == H2O_NH3_ID || item->data_id == H2O_TURBIDITY_ID || item->data_id == H2O_CL2_ID || item->data_id == ORP_POTENTIOMETER_ID || item->data_id == YMSD_ID)
	{
		data[4] = 0x00;
		data[5] = 0x02;
	}
	else if(item->data_id == RJY_ID)	//Dissolved Oxygen 4字节
	{
		data[4] = 0x00;
		data[5] = 0x04;
	}
	else if(item->data_id == CHLOROPHYLL_ID)	//Chlorophyll 5字节
	{
		data[4] = 0x00;
		data[5] = 0x05;
	}
	else
	{
		data[4] = 0x00;
		data[5] = 0x01;
	}
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;	
	
	Uart_Send_Data(SENSOR485_COM, data, 8);
}

void Uart_Set_485Addr(USART_TypeDef* USARTx, unsigned char sAddr, unsigned char dAddr)
{
	static unsigned char data[8] = {0x02, 0x06, 0x20, 0x00, 0x00, 0x01, 0xC2, 0X38};
	unsigned short CRCVal = 0;
	
	data[0] = sAddr;	//源地址
	data[1] = 0x06;		//修改地址功能码
	data[2] = 0x00;		//寄存器地址高位
	data[3] = 0x00;		//寄存器地址低位
	data[4] = 0x00;		//目标地址高位
	data[5] = dAddr;	//修改后的地址
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;
	Uart_Send_Data(USARTx, data, 8);	
}

void Uart_Inquire_485Addr(USART_TypeDef* USARTx)
{
	static unsigned char data[8] = {0x00, 0x06, 0x20, 0x00, 0x00, 0x00, 0xC2, 0X38};
	unsigned short CRCVal = 0;
	
	data[0] = 0x00;	//源地址
	data[1] = 0x09;		//查询地址功能码
	data[2] = 0x00;		//寄存器地址高位
	data[3] = 0x01;		//寄存器地址低位
	data[4] = 0x00;		//目标地址高位
	data[5] = 0x00;	//修改后的地址
	CRCVal = Calculate_CRC16(data, 6);
	data[6] = CRCVal&0xff;
	data[7] = CRCVal >> 8;
	Uart_Send_Data(USARTx, data, 8);	
}


unsigned int Wait485SensorData(MAP_IN_ITEM *item, unsigned char *ErrorFlag, float *f_Val1, float *f_Val2)
{
	unsigned int s_LastTime = 0, CRCVal = 0;
	unsigned int Result = 0;
	unsigned char data[100], i, Num = 0;
	
	s_LastTime = GetSystem10msCount();
	while(1)
	{
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			
			if(g_USART2_RX_CNT >= 100)
			{
				return ERROR_STATE_OVERDATA;
			}
			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				data[i] = g_USART2_RX_BUF[i];
//				u1_printf("%02X ", data[i]);
			}
			
			if(item->data_id == KQW_ID || item->data_id == KQS_ID || item->data_id == GZD_ID || item->data_id == AIR_PRESS_ID || item->data_id == H2O_NH3_ID || \
				item->data_id == H2O_TURBIDITY_ID || item->data_id == H2O_CL2_ID || item->data_id == RJY_ID || item->data_id == SOIL_M_ID || item->data_id == TRW_ID || item->data_id == CHLOROPHYLL_ID)
			{
				Num = data[2];
				
				if(Num == 4)	//4字节浮点数
				{
					CRCVal = Calculate_CRC16(data, 7);
					if(CRCVal == ((data[8] << 8) + data[7]))
					{

						if((data[3] << 24) + (data[4] << 16) + (data[5] << 8) + data[6] == ERROR_485VALUE)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
							Result = ERROR_485VALUE;
						}
						else
						{
							memcpy(f_Val1, (void *)&data[3], sizeof(float));
							*ErrorFlag = ERROR_STATE_NORMAL;
						}
//						u1_printf("%02X %02X %02X %02X\r\n", data[3] ,data[4] ,data[5] ,data[6]); 
					}
					else
					{
//						u1_printf("\r\n ID:%02X:HY Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 8)	//Dissolved Oxygen
				{
					CRCVal = Calculate_CRC16(data, 11);
					if(CRCVal == ((data[12] << 8) + data[11]))
					{
						memcpy(f_Val1, (void *)&data[3], sizeof(float));	
						memcpy(f_Val2, (void *)&data[7], sizeof(float));	
						*ErrorFlag = ERROR_STATE_NORMAL;
						u1_printf("%2.2f %2.3f%% ", *f_Val1, *f_Val2); 
					}
					else
					{
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 10)	//Chlorophyll
				{
					CRCVal = Calculate_CRC16(data, 13);
					if(CRCVal == ((data[14] << 8) + data[13]))
					{
						memcpy(f_Val1, (void *)&data[3], sizeof(float));	
						memcpy(f_Val2, (void *)&data[7], sizeof(float));	
						*ErrorFlag = ERROR_STATE_NORMAL;
						u1_printf("%2.2f %2.3fug/L", *f_Val1, *f_Val2); 
					}
					else
					{
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else if(Num == 2)	//其他传感器2字节
				{
					CRCVal = Calculate_CRC16(data, 5);
					if(CRCVal == ((data[6] << 8) + data[5]))
					{
						Result = (float)((data[3] << 8) + data[4]);
						*f_Val1 = Result;
						if(Result == ERROR_SENSOR_DATA)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
						}
					}
					else
					{
//					u1_printf("\r\n ID:%02X:Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else
				{
//					u1_printf(" error num:%d\r\n", Num);
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}
			}
			else
			{
				Num = data[2];
				
				if(Num <= 4)
				{
					Result = 0;
					CRCVal = Calculate_CRC16(data, Num+3);
					if(CRCVal == ((data[4+Num] << 8) + data[3+Num]))
					{
						for(i=0; i<Num; i++)
						{
							if(item->data_id == ORP_POTENTIOMETER_ID)
							{
								Result = ((data[3] << 8) + data[4]);
								*f_Val1 = (float)Result;
								
								if(Result > 10000)	//是负数
								{
									Result = 0xFFFF - Result;
									*f_Val1 = 0.0 - (float)Result;
								}
							}
							else if(item->data_id == YMSD_ID)		//叶面湿度传感器						
							{
								Result = ((data[3] << 8) + data[4]);
							}
							else 
							{
								Result += data[3+i] << (8*(Num- i - 1));
							}
						}
						if(Result == ERROR_SENSOR_DATA)
						{
							*ErrorFlag = ERROR_STATE_SENSORERROR;
						}
					}
					else
					{
//						u1_printf("\r\n ID:%02X:Sensor CRC Error", SensorID);
						*ErrorFlag = ERROR_STATE_CRCERROR;
						Result = ERROR_485VALUE;
					}
				}
				else
				{
//					u1_printf("\r\n ID:%02X:Num Error", SensorID);
					*ErrorFlag = ERROR_STATE_CRCERROR;
					Result = ERROR_485VALUE;
				}
			}	
			break;
		}
	
		
		if(item->data_id == PH_ID || item->data_id == RJY_ID || item->data_id == AIR_ION)
		{
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50) 	//PH 延时长
			{
				s_LastTime = GetSystem10msCount();					
//				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
				Result = ERROR_485VALUE;
				*ErrorFlag = ERROR_STATE_OVERTIME;
				break;
			}	
		}
		else if(item->data_id == CHLOROPHYLL_ID)
		{
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 150) 	//Chlorophyll
			{
				s_LastTime = GetSystem10msCount();					
//				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
				Result = ERROR_485VALUE;
				*ErrorFlag = ERROR_STATE_OVERTIME;
				break;
			}	
		}
		else
		{
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 25) 
			{
				s_LastTime = GetSystem10msCount();					
//				u1_printf("\r\n Get %02X Sensor OverTime", SensorID);
				Result = ERROR_485VALUE;
				*ErrorFlag = ERROR_STATE_OVERTIME;
				break;
			}		
		}		
		IWDG_ReloadCounter();	
	}		
	
	Clear_Uart2Buff();
	
	return Result;
}
//等待慧云传感器修改地址指令应答
unsigned char Wait485SensorAck(void)
{
	unsigned int s_LastTime = 0, CRCVal = 0;
	unsigned int Result = 0;
	unsigned char data[9], i, SensorID, Function;
	
	s_LastTime = GetSystem10msCount();
	while(1)
	{
		if(g_Uart2RxFlag == TRUE)
		{
			g_Uart2RxFlag = FALSE;
			for(i=0; i<9; i++)
			{
				data[i] = g_USART2_RX_BUF[i];
			}
			
			SensorID = data[0];
			Function = data[1];
			
			if(Function == CMD_FUNCTION)		//修改地址功能码
			{
				CRCVal = Calculate_CRC16(data, 6);
				if(CRCVal == ((data[7] << 8) + data[6]))
				{
					u1_printf("\r\n Sensor Addr (0x%02X) to (0x%02X) succeed!\r\n", data[0], data[5]);		
				}
				else
				{
					u1_printf("\r\n ID:%02X :CRC error", SensorID);
					Result = ERROR_485VALUE;
					for(i=0; i<8; i++)
					{
						u1_printf(" %02X", g_USART2_RX_BUF[i]);
					}
					u1_printf("\r\n");
				}
			}
			else if(Function == CMD_INQRIRE)		//查询地址功能码
			{
				CRCVal = Calculate_CRC16(data, 6);
				if(CRCVal == ((data[7] << 8) + data[6]))
				{
					u1_printf("\r\n RS485 Addr:%02X\r\n", data[2]);
				}
				else
				{
					u1_printf("\r\n Inquire Addr Error: ");
					Result = ERROR_485VALUE;
					for(i=0; i<8; i++)
					{
						u1_printf(" %02X", g_USART2_RX_BUF[i]);
					}
					u1_printf("\r\n");
				}
			}
			break;
		}
		
		if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
		{
			s_LastTime = GetSystem10msCount();					
			u1_printf("\r\n Change Addr Over time\r\n");
			Result = ERROR_485VALUE;
			break;
		}
	}		
	Clear_Uart2Buff();
	return Result;
}
//获取传感器数据
float Get485SensorValue(MAP_IN_ITEM *item, unsigned char *Err)
{
	volatile float f_Result = 0.0, f_Return = 0.0;
	uint32 ErrValue = ERROR_485VALUE;
	float f_Val1 = 0, f_Val2 = 0, X1, X2;
	unsigned char ErrorFlag = 0;
	
	double pressure = 101.3;
	double Phmg= 0.0;
	double T = 0.0;
	double u = 0.0;

	memcpy((uint8 *)&f_Return, (uint8 *)&ErrValue, sizeof(uint32));
	*Err = ERROR_STATE_NORMAL;
	
	Uart_Send_485CMD(item);
	
	f_Result = Wait485SensorData(item, &ErrorFlag, &f_Val1, &f_Val2);
	
	if(ErrorFlag == ERROR_STATE_OVERTIME)
	{
//		u1_printf(" Err1 ");
		*Err = ERROR_STATE_OVERTIME;
		return f_Return;
	}
	else if(ErrorFlag == ERROR_STATE_SENSORERROR)
	{
		u1_printf(" Err2 ");
		*Err = ERROR_STATE_SENSORERROR;
		return f_Return;
	}
	else if(ErrorFlag == ERROR_STATE_CRCERROR)
	{
		u1_printf(" Err3 ");
		*Err = ERROR_STATE_CRCERROR;
		return f_Return;
	}
	
	switch(item->data_id)
	{
//		特殊处理
		case TRW_ID:		//Soil Temperature
			if(item->in_param[4] == TRW_ID)	
			{
				f_Result = f_Val1;
			}
			else
			{
				if(f_Result < 8000)		//注意  Soil Temperature与Liquid Level的算法不同,Soil Temperature正负以8000为临界,Liquid Level以0x8000为临界
				{
					f_Result = f_Result/10.0;
				}
				else		//负数
				{
					f_Result = (0xffff - f_Result + 1)/10.0;
					f_Result = -f_Result;
				}	
			}			
		break;	

		case RJY_ID:	//Dissolved Oxygen转换公式
			
			T = 273.15 + f_Val1;
			X1 = -173.4292+249.6339*(100.0/T)-21.8492*(T/100.0)+(log(T/100.0))*143.3483;
			X1 = exp(X1);
			u = 8.10765 - (1750.286/ (235 + f_Val1));// u� = log u
			u = pow(10, u); 
			Phmg = pressure*760.0/101.325; 
			X2  =  ((Phmg - u)/(760 - u)); 
		
			f_Result = f_Val2*X1*X2*1.4276;
		
			if(f_Result < 0 && f_Result > -1)	//接近于0时有可能出现负值
			{
				f_Result = 0;
			}
			
		break;
		
		case ORP_POTENTIOMETER_ID:	//氧化还原
			f_Result = f_Val1;
		break;
		
		case CHLOROPHYLL_ID:		//Chlorophyll
			f_Result = f_Val2;
		
			if(f_Result < 0 && f_Result > -1)	//接近于0时有可能出现负值
			{
				f_Result = 0;
			}
		break;
		
		case YW_ID:					//Liquid Level

		
			f_Result = f_Result/2000.0;		//星仪Liquid Level传感器
		break;
//		读数不处理			
		case SOIL_YF_ID:			//Soil Salinity值
		
			//Nothing
		break;
//		读数/10			
		case TRS_ID:		//Soil Humidity
		case FX_ID:			  //Wind Direction
		case FS_ID:			  //Wind Speed
		case O2_ID:
		case CO2_PER_ID:
		case YMSD_ID:
			f_Result = f_Result/10.0;
		break;	
//		慧云传感器
		case GZD_ID:			//Illuminance

			if(item->in_param[5])	//迪辉传感器
			{
				f_Result = f_Result*10;
			}
			else			// 慧云传感器
			{
				f_Result = f_Val1;
				
			}
			
		break;

		case AIR_PRESS_ID:			//Atmospheric Pressure			
		
			if(item->in_param[5])	//迪辉传感器
			{
				f_Result = f_Result/10.0;
			}
			else			// 慧云传感器
			{
				f_Result = f_Val1;
				
			}
		break;
		
		case KQW_ID:			//Air Temperature
	
			if(item->in_param[5])	//迪辉传感器
			{
				if(f_Val1 < 8000)		
				{
					f_Result = f_Val1/10.0;
				}
				else		//负数
				{
					f_Result = (0xffff - f_Val1)/10.0;
					f_Result = -f_Result;
				}
			}
			else			// 慧云传感器
			{
				f_Result = f_Val1;
				
			}
		break;		
		
		case KQS_ID:			//空气湿度				
			if(item->in_param[5])	//迪辉传感器
			{
				f_Result = f_Val1/10.0;
			}
			else			// 慧云传感器
			{
				f_Result = f_Val1;
				
			}
		break;
			
		case H2O_TURBIDITY_ID:
		case H2O_NH3_ID:
		case H2O_CL2_ID:
			f_Result = f_Val1;
			f_Result = encode_float(f_Result);	
		break;
		
		case SYL_ID:		//Water Pressure  0~1.6Mpa量程
			f_Result = 1.6*f_Result/2.0;		//星仪压力计	*1000 显示为Kpa  20190703 gys
		break;
//		读数/100		
		case PH_ID:			//PH Value
		case DDL_ID:		//Water Electric
		case SOIL_EC_ID:			//Soil Electric
		case NH3_NH4_ID:
			f_Result = f_Result/100.0;
		break;
//Soil Tension    电流型或SDI-12		
		case SOIL_M_ID:
			if(item->in_param[4] == SOIL_WATER_POTENTIAL)		
			{
				f_Result = -f_Val1;
			}
			else
			{
				f_Result = f_Result/100.0;
			}
		break;	
		
		default:
		
		break;
	
	}	
	
	return f_Result;
}


void Init_485Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR485_ENABLE_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SENSOR485_ENABLE_GPIO_TYPE, &GPIO_InitStructure);	
	
	SENSOR485_ENABLE();

}

void Deinit_485Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR485_ENABLE_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SENSOR485_ENABLE_GPIO_TYPE, &GPIO_InitStructure);	
	
	SENSOR485_DISABLE();
	
}


unsigned char Get_485Sensor_Data(unsigned char ChannelNum)
{
	MAP_IN_ITEM *item;
	uint32 error_mark = 0xEEEEEEEE;
	u8 i, RertyCount = 2, Err = 0;
	
	if(	hwl_input_init(ChannelNum, FALSE) )
	{			
		item = get_map_in_item(ChannelNum);
		
		//根据通道读取传感器数据,数据与通道一一对应
		SensorChannelData.ChannelData[ChannelNum] = Get485SensorValue(item, &Err);
		if(item->data_id == YW_ID)
		{
			RertyCount = 20;
		}
		else if(item->data_id == CHLOROPHYLL_ID || item->data_id == YMSD_ID)
		{
			RertyCount = 10;
		}
		else
		{
			RertyCount = 2;
		}
		for(i=0; i<RertyCount; i++)
		{
			if(Err > 0)
			{
				Clear_Uart2Buff();
				delay_ms(150);
				SensorChannelData.ChannelData[ChannelNum] = Get485SensorValue(item, &Err);
			}
			else
			{
				break;
			}
		}
		
	
		ShowSensorName(item->data_id, TRUE, SensorChannelData.ChannelData[ChannelNum]);

		
		switch(item->data_id)
		{						
			case YW_ID:			  //Liquid Level
				if(SensorChannelData.ChannelData[ChannelNum] == ERROR_485VALUE)
				{
				}
				else
				{
					switch(item->in_param[1])	//每个脉冲对应的Flow数值是多少。
					{
						case 0:
							SensorChannelData.ChannelData[ChannelNum] *= 2;  //2m
							break;
						case 1:
							SensorChannelData.ChannelData[ChannelNum] *= 5;	//5m	
							break;
						case 2:
							SensorChannelData.ChannelData[ChannelNum] *= 10;//10m	
							break;
						case 3:
							SensorChannelData.ChannelData[ChannelNum] *= 20;//20m	
							break;
						case 4:
							SensorChannelData.ChannelData[ChannelNum] *= 50;//50m	
							break;
					}
					
					u1_printf("\r\n Depth:%.2f m\r\n", SensorChannelData.ChannelData[ChannelNum]);
				}
			break;											
		}
		
		if(Err > 0)
		{		
//			SetLogErrCode(LOG_CODE_SENSORERR);
			if(SensorChannelData.SensorOKStatus[ChannelNum])
			{
				if(SensorChannelData.ErrorCount[ChannelNum] >= 1)
				{
					memcpy((uint8 *)&SensorChannelData.ChannelData[ChannelNum], (uint8 *)&error_mark, sizeof(uint32));
				}				
				else
				{
					SensorChannelData.ErrorCount[ChannelNum]++;
					SensorChannelData.ChannelData[ChannelNum] = SensorChannelData.LastChannelData[ChannelNum];
				}
			}
			else
			{
				memcpy((uint8 *)&SensorChannelData.ChannelData[ChannelNum], (uint8 *)&error_mark, sizeof(uint32));
				
			}		
		}
		else	//存储上一次正常数据
		{
			SensorChannelData.LastChannelData[ChannelNum] = SensorChannelData.ChannelData[ChannelNum];
			SensorChannelData.ErrorCount[ChannelNum] = 0;
			SensorChannelData.SensorOKStatus[ChannelNum] = TRUE;
		}
		
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static void SpecialSensorStartCMD(void)
{
	u8 i, Addr = 0, Err = 0;
	
	if(GetRJYSensorFlag(&Addr))
	{
		for(i=0; i<10; i++)
		{
			Err = Uart_Send_StartMeasure(Addr);
			if(!Err)
			{
				Clear_Uart2Buff();
				delay_ms(100);
			}
			else
			{
				u1_printf("\r\n Start Dissolved Oxygen Measure Fail\r\n");
				break;
			}
			IWDG_ReloadCounter();	
		}		
		if(i == 10)
		{
			u1_printf("\r\n Start Dissolved Oxygen Measure Fail\r\n");
		}
	}
	
	if(GetChlorophyllSensorFlag(&Addr))
	{
		for(i=0; i<10; i++)
		{
			Err = Uart_Send_StartMeasure(Addr);
			if(!Err)
			{
				Clear_Uart2Buff();
				delay_ms(100);
			}
			else
			{
				u1_printf("\r\n Start Chlorophyll Measure Fail\r\n");
				break;
			}
			IWDG_ReloadCounter();	
		}		
		if(i == 10)
		{
			u1_printf("\r\n Start Chlorophyll Measure Fail\r\n");
		}
	}
}


void TaskFor485Sensor(void)
{
	static unsigned int s_LastTime = 0;
	static u8 s_State = 1, s_ChannelNum = 0, s_IsTrue = 0, s_WaitStartTime = 0;
	static u16 s_Data_interval = 0, s_LastGetSensorDataTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	u8  Addr = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	if(CalculateTime(GetSystem10msCount(), s_LastGetSensorDataTime) >= p_sys->State_interval*100)
	{
		s_LastGetSensorDataTime = GetSystem10msCount();
		StartGetBatVol();
		g_485SensorGetFlag = FALSE;
	}
	
	if(g_485SensorGetFlag == FALSE)
	{
		switch(s_State)
		{
			case 1:			//开启传感器电源,初始化485串口
				Init_485Port();	
				USART2_Config(SENSOR485_BAND_RATE);
				s_LastTime = GetSystem10msCount();
				
				if(GetChlorophyllSensorFlag(&Addr))
				{
					s_WaitStartTime = 7;
					if(s_Data_interval < s_WaitStartTime)
					{
						s_Data_interval = 30;						
					}
					s_State = 2;
				}
				else if(GetRJYSensorFlag(&Addr))
				{
					s_WaitStartTime = 2;
					if(s_Data_interval < s_WaitStartTime)
					{
						s_Data_interval = 20;						
					}
					s_State = 2;
				}
				else
				{
					s_State = 3;
				}
				
			break;
			
			case 2:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_WaitStartTime*100)
				{
					u1_printf(" Start Measure\r\n");
					SpecialSensorStartCMD();
					s_State = 3;
				}
			break;
			
			case 3:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= g_ReportDataWaitingTime*100 + 20) //延时等待传感器数据
				{
					Clear_Uart2Buff();
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]----Original Data:----\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);					
					s_State++;
					s_LastTime = GetSystem10msCount();
					s_ChannelNum = 0;
				}	
			break;
				
			case 4:		//输出传感器原始数据
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= SAMPLING_INTERVAL)
				{
					s_IsTrue = Get_485Sensor_Data(s_ChannelNum++);
					if(s_IsTrue)
					{
						s_LastTime = GetSystem10msCount();
					}
				}
				
				if(s_ChannelNum >= HWL_INPUT_COUNT)
				{
					s_ChannelNum = 0;
	//				CheckSensorDataForControl();
					s_State = 5;
					g_485SensorGetFlag = TRUE;						
				}
			break;	

			case 5:
				
				s_State = 3;
			break;
		}
	}	
}


void Sensor485_Init(void)
{
	u8 i;
	SYSTEMCONFIG *p_sys;
	
		
	for (i=0;i<HWL_INPUT_COUNT;i++)
	{
		if(hwl_input_init(i, TRUE))		//读取输入通道配置信息
		{
			s_InputChannelCount++;		
		}
	}
	delay_ms(10);
	p_sys = GetSystemConfig();
	if(p_sys->State_interval == 0)
	{
		u1_printf("\r\n Wait Interval:%ds\r\n", g_ReportDataWaitingTime);
	}
	else
	{
		u1_printf("\r\n Wait Interval:%ds\r\n", p_sys->State_interval);
		g_ReportDataWaitingTime = p_sys->State_interval;
		
	}
	
	if(g_ReportDataWaitingTime > 100)//最大等待时长为100s,过长的等待没有意义,不如直接保持长连接
	{
		g_ReportDataWaitingTime = 100;
	}
		
	if(s_InputChannelCount)
	{
		u1_printf("\r\n Collector Init Finish,Channel Num:%d...\r\n", s_InputChannelCount);
	}
	else
	{
		g_485SensorGetFlag = TRUE;
	}
	
	if(s_InputChannelCount == 0)
	{
		return;
	}
	

}













