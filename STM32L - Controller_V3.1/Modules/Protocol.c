#include "Protocol.h"
#include "main.h"

#define OUTPUT_INDEX_BASE		(32)

void Mod_Send_Ack(USART_TypeDef* USARTx, CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char Pack[128];
	unsigned int len = 0;

	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = hdr->device_id >> 24;
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no ;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	u1_printf(" [发送服务端ACK] from %d(%d) ->: ", hdr->device_id, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	

	Uart_Send_Data(USARTx, Pack, nMsgLen);		
}


unsigned int Mod_Report_Data(USART_TypeDef* USARTx)
{
	u16 nMsgLen;
	static u16 seq;
	unsigned int send_len;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256];
//	DATA_INFO data_info;
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);			//通信版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);	  //设备号
	hdr->dir = 0;												  //方向
	hdr->seq_no = swap_word(seq);							  //包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 0;
	

	
//	for (i=0;i<HWL_INPUT_COUNT;i++)
//	{
//		if (hwl_get_input_value(i, &data_info))
//		{
//			ch = (i+1);
//			val_u16 = swap_word(ch);		//通道
//			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
//			ptr += sizeof(uint16);
//			val_u16 = swap_word(data_info.data_id);//类型
//			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
//			ptr += sizeof(uint16);
//			(*ptr) = MARK_FLOAT;	//数据标识
//			ptr++;
//			val_float = encode_float(data_info.value);	//数据
//			memcpy(ptr, (void *)&val_float, sizeof(float));
//			ptr += sizeof(float);
//			count++;
//		}
//	}	
	
	val_u16 = swap_word(count+1);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	count++;	

	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,100);
	//发送消息
	u1_printf("\r\n [Mod][Data] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Packet[i]);
	}
	u1_printf("\r\n");	

	Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	return nMsgLen;	
}


void Mod_Send_Alive(USART_TypeDef* USARTx)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);							//通信协议版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度


	send_buf[len++] = 84;	//电池电压
	
	Int32ToArray(&send_buf[len], p_sys->Device_ID);
	len +=4;
	
	send_buf[len++] = 20;	//信号强度

	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AlivePack, 100);		//数据包转义处理	
	u1_printf("\r\n [Mod][Heart] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	

	Uart_Send_Data(USARTx, AlivePack, nMsgLen);	
}

//读取设备通信配置信息 --0x04 =========================================
unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG  *com_sys_cfg;
	
	
	memcpy(send_buf, pMsg, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->protocol = swap_word(hdr->protocol);	
	hdr->device_id = swap_dword(hdr->device_id);
	hdr->seq_no = swap_word(hdr->seq_no);	
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);
	}
	else
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);
	}
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_topology = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);		
	
	send_len+=sizeof(SYSTEMCONFIG)+4;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息

//	if(USARTx == LORA_COM)
//		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
//	else 
//		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	}
	
	u1_printf("[Lora][系统配置上报] -> %d (%d):", sys_cfg->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	
	u1_printf("\r\n");	
	return 1;
}

//写入通道配置信息 0x05 ===============================================
unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	
	SYSTEMCONFIG new_sys_cfg;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG *com_sys_cfg;
	
	uint16 val_u16;
	unsigned int val_u32;
	unsigned int payload_len;

	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	ptr = &data[sizeof(CLOUD_HDR)];
	hdr = (CLOUD_HDR *)data;
	
	payload_len = swap_word(hdr->payload_len);
	
	sys_cfg = GetSystemConfig();
	memcpy(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG));
	
	while(payload_len)
	{
		switch((*ptr))
		{
			case CFG_ID_1:	  	//系统信息分组ID
				ptr++;
				payload_len--;
				val_u32 = *(unsigned int *)ptr;
				new_sys_cfg.Device_ID = (swap_dword(val_u32)) & 0xfffffff ;
				ptr += sizeof(unsigned int);
				new_sys_cfg.Type = (*ptr);
				ptr++;
				payload_len-=5;

				u1_printf("[设置]:设备编号=%u,网络连接类型=%d\n",new_sys_cfg.Device_ID,new_sys_cfg.Type );
				if((new_sys_cfg.Type != NETTYPE_LORA) && (new_sys_cfg.Type != NETTYPE_SIM800C) && (new_sys_cfg.Type != NETTYPE_GPRS) )
				{
					u1_printf("\r\n 网络类型错误\r\n");
					return 0;
				}
				break;
			case CFG_ID_2: 	//Zigbee信息分组ID
				ptr++;
				payload_len--;
			
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Net = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Node = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gateway = swap_word(val_u16);
				ptr += sizeof(uint16);
				new_sys_cfg.Channel = (*ptr);
				ptr++;
				new_sys_cfg.LowpowerFlag = (*ptr);
				ptr++;
				payload_len-=8;

				u1_printf("[设置]:Lora网络号=%u,节点=%d,主机=%d,频道=%d,拓扑=%d\n"
						,new_sys_cfg.Net
						,new_sys_cfg.Node
						,new_sys_cfg.Gateway
						,new_sys_cfg.Channel
						,new_sys_cfg.LowpowerFlag
						 );
				if(new_sys_cfg.Channel > 32)
				{
					u1_printf("\r\n 频道错误\r\n");
					return 0;
				}
				if(new_sys_cfg.Gateway != new_sys_cfg.Net)
				{
					u1_printf("\r\n 网关号与网络号不符合\r\n");
					return 0;
				}
				
//				if(new_sys_cfg.Node != ((new_sys_cfg.Device_ID) & 0xfffffff))
//				{
//					u1_printf("\r\n 节点号与设备ID不符合\r\n");
//					return 0;
//				}
				
				break;
			case CFG_ID_3:		//GPRS分组信息ID
				ptr++;
				payload_len--;
			
				memcpy(new_sys_cfg.Gprs_ServerIP, ptr, 4);
				ptr += 4;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gprs_Port = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=6;

				u1_printf("[设置]:GPRS服务器地址=%u.%u.%u.%u,端口=%d\n"
						,(new_sys_cfg.Gprs_ServerIP[0])
						,(new_sys_cfg.Gprs_ServerIP[1])
						,(new_sys_cfg.Gprs_ServerIP[2])
						,(new_sys_cfg.Gprs_ServerIP[3])
						,new_sys_cfg.Gprs_Port
						 );
				break;
			case CFG_ID_4: 	//通信参数分组ID
				ptr++;
				payload_len--;
			
				new_sys_cfg.Retry_Times = (*ptr);
				ptr++;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Heart_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Data_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.State_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=7;

				u1_printf("[设置]:重发次数=%u.心跳间隔=%u.数据间隔=%u.状态间隔=%u\n"
						,new_sys_cfg.Retry_Times
						,new_sys_cfg.Heart_interval
						,new_sys_cfg.Data_interval
						,new_sys_cfg.State_interval
						 );
						 
				if(new_sys_cfg.Heart_interval > 30000)
				{
					u1_printf(" 心跳间隔超量程\r\n");
					return 0;
				}
				
				if(new_sys_cfg.Data_interval > 30000)
				{
					u1_printf(" 数据间隔超量程\r\n");
					return 0;
				}
				
				if(new_sys_cfg.State_interval > 30000)
				{
					u1_printf(" 状态间隔超量程\r\n");
					return 0;
				}
				
				break;
			default:
				ptr++;
				payload_len--;
				break;
		}
	}
	
	if (memcmp(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG)))
	{
		u1_printf("[设置]:存储设备通信配置\n");
		Set_System_Config(&new_sys_cfg);
	}
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
	send_len = sizeof(CLOUD_HDR);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);
	}
	else
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);
	}
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_topology = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);	
	
	send_len+=sizeof(SYSTEMCONFIG)+4;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息
	u1_printf("[Lora][更新系统配置上报] -> %d (%d):",  sys_cfg->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", send_buf[i]);
	}	
	u1_printf("\r\n");
	
//	if(USARTx == LORA_COM)
//		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
//	else 
//		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	}
	
	return 1;
}


////查询输出状态-0x0002 ======================================================
unsigned int Debug_Get_Control_State(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[128], Packet[128];
	unsigned int send_len, nMsgLen;
	unsigned int payload_len;
	
	CLOUD_HDR *hdr;				//数据包头格式
	unsigned char *ptr;
	unsigned char count;
	CHANNEL_STATUS ch_status;	 //通道状态
	unsigned short ch,val_u16;
	int i;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;												  //方向
	
	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;

	count = 0;
	for (i=0;i<OUTPUT_CHANNEL_COUNT;i++)
	{
		if (output_get_status(i, &ch_status))  	//读取输出通道的运行状态
		{
			ch = i+OUTPUT_INDEX_BASE+1;
			val_u16 = swap_word(ch);
			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
			ptr += sizeof(unsigned short);
			val_u16 = swap_word(ch_status.ch_type);
			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
			ptr += sizeof(unsigned short);
			(*ptr) = ch_status.afc_type;
			ptr++;
			(*ptr) = ch_status.is_manual;
			ptr++;
			(*ptr) = ch_status.mark;
			ptr++;
			(*ptr) = *((unsigned char *)ch_status.content);
			ptr++;
			count++;
		}
	}
	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
//	send_buf[send_len] = cloud_calc_chksum(send_buf, send_len);
//	send_len++;

//	zigbee_target = g_sys_ctrl.sys_cfg->zigbee_gateway;			//网关地址
//	cloud_send_packet( uart_no,send_buf,send_len );			//端口在发送的时候选择
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	nMsgLen = PackMsg( send_buf,send_len,Packet,100);

//	if(USARTx == LORA_COM)
//		Lora_Send_Data(p_sys->Net, p_sys->Channel, Packet, nMsgLen);
//	else 
//		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	
	if(p_sys->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(p_sys->Net, p_sys->Channel, Packet, nMsgLen);
	}
	else
	{
		Uart_Send_Data(USARTx, Packet, nMsgLen);	
	}
	
	return 1;	
	
}

//手动控制执行操作(0x0003) =============================================
unsigned int Debug_Manual_Ctrl(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[100], Packet[100];
	unsigned int send_len, nMsgLen;
	
	CLOUD_HDR *hdr;
	unsigned char *action;	  //动作
	unsigned char *ptr;
	unsigned short channel;	  //通道
	unsigned short duration;	//持续时间
	unsigned char start_action;	//起始动作
	unsigned char nPos = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	hdr = (CLOUD_HDR *)data;	   //data----接收数据指针
	send_len = sizeof(CLOUD_HDR)+(hdr->payload_len);	
	hdr->protocol = swap_word(hdr->protocol);			//通信版本号
	hdr->device_id = swap_dword(hdr->device_id);	  //设备号
	hdr->seq_no = swap_word(hdr->seq_no);							  //包序号
	hdr->payload_len = swap_word(hdr->payload_len);							  //包序号
	//命令字		

	memcpy(send_buf, data, send_len);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;				 	
	
	nPos = sizeof(CLOUD_HDR);
	channel = *(unsigned short *)&data[nPos];		//通道

	channel = swap_word(channel) - OUTPUT_INDEX_BASE - 1;
	
	nPos += 2;
	
	//跳过设备类型
	nPos+=2;
	//执行的动作
	action = (unsigned char *)&data[nPos];	//模式
	ptr = action;
	ptr++;
	
	duration = ((*ptr) << 8) + (*(ptr+1));	//时长
	ptr+=2;
	
	ptr++;				//特征码 0X05
	
	start_action = *ptr;	//启动动作
	
	ptr++;				//特征码 0X05
		
	//调试信息
//	u1_printf("[控制]: 通道=%d, 模式=%d,动作=%d,持续时间=%d分钟,起始动作=%d,结束动作=%d\r\n"
//				, channel+1, controlMode, start_action, duration,start_action,end_action);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	if(start_action == 1)
	{
		u1_printf(" [控制]: 通道(%d)开启,持续时间：%d min\r\n", channel+1, duration);
		SetLogErrCode(LOG_CODE_MOTOR_ON);
		StoreOperationalData();
		if(duration >= 1440)
		{
			u1_printf("----!!警告,时间大于1440min,控制器将不会关闭!!----\r\n");
		}
	}
	else if(start_action == 2)
	{
		u1_printf(" [控制]: 通道(%d)关闭,持续时间：%d min\r\n", channel+1, duration);
		SetLogErrCode(LOG_CODE_MOTOR_CLOSE);
		StoreOperationalData();
		if(duration >= 1440)
		{
			u1_printf("----!!警告,时间大于1440min,控制器将不会关闭!!----\r\n");
		}
	}
	else
	{
		SetLogErrCode(LOG_CODE_MOTOR_OFF);
		StoreOperationalData();
		u1_printf(" [控制]: 通道(%d)停止\r\n", channel+1);
	}
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	

	nMsgLen = PackMsg(send_buf, send_len, Packet, 120);	

	if(USARTx == USART1)
	{
		delay_ms(10);
		Uart_Send_Data(USARTx, Packet, nMsgLen);
	}
	
	output_manual_action(channel, action);	//channel是0--11
	
	return 1;
}

unsigned int Debug_Report_Status(USART_TypeDef* USARTx)
{
	unsigned char send_buf[128], Packet[128], WaitTime = 0;
	unsigned int send_len, payload_len, nMsgLen;
	RTC_TimeTypeDef RTC_TimeStructure;
	CLOUD_HDR *hdr;				//数据包头格式
	unsigned char *ptr, count = 0;
	CHANNEL_STATUS ch_status;	 //通道状态
	unsigned short ch,val_u16;
	static unsigned short seq_no = 0;
	unsigned int i, OverTime = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();		
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_CONTROL_ID);		//通信协议版本
	hdr->device_id = swap_dword(g_SysCtrl.SysConfig->Device_ID);	 	//设备号
	hdr->dir = 0;							  //通信方向
	hdr->seq_no = swap_word(seq_no);		//通信序号
	seq_no++;								//通信包序号
	hdr->cmd = CMD_REPORT_S;				//命令字
	
	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 0;
	for (i=0;i<OUTPUT_CHANNEL_COUNT;i++)
	{
		if (output_get_status(i, &ch_status))  	//读取输出通道的运行状态
		{
			ch = i+OUTPUT_INDEX_BASE+1;		//通道号
			val_u16 = swap_word(ch);
			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
			ptr += sizeof(unsigned short);
			val_u16 = swap_word(ch_status.ch_type);	//类型
			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
			ptr += sizeof(unsigned short);
			(*ptr) = ch_status.afc_type;	//控制类型,0--本地,1--远程
			ptr++;
			(*ptr) = ch_status.is_manual;
			ptr++;
			(*ptr) = ch_status.mark;
			ptr++;
			(*ptr) = *((unsigned char *)ch_status.content);
			ptr++;
			count++;
		}
	}
	
	val_u16 = swap_word(1);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);	
	count++;
		
	if (count == 0) return 0;
	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);	

	send_buf[send_len] = Calc_Checksum(send_buf, send_len);		//数据包和校验
	send_len++;

	nMsgLen = PackMsg(send_buf, send_len, Packet, 100);		//数据包转义处理
	
	
	OverTime = 0;
	if(p_sys->Type == NETTYPE_LORA)
	{
		OverTime = 0;
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		while(1)	
		{
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
			WaitTime = RTC_TimeStructure.RTC_Seconds%10;
			if(WaitTime >= 1)	//2~9s为控制器通信时区
			{
				break;
			}
			OverTime++;
			if(OverTime >= 100000)
			{
				u1_printf(" Over Time!\r\n");
				break;
			}
		}
		Lora_Send_Data(p_sys->Net, p_sys->Channel, Packet, nMsgLen);
	}
	else 
	{
		USART3_DMA_Send(Packet, nMsgLen);	
	}
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	u1_printf("\r\n [%02d:%02d:%02d][%d][Status] -> (%d) ",  RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, p_sys->Device_ID,  p_sys->Gateway);

	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ",Packet[i]);
	}

	u1_printf("\r\n");		   

	return 1;
}

