#ifndef	_SENSORMANAGE_H_
#define _SENSORMANAGE_H_

#include "map_in.h"

#define		HWL_INPUT_COUNT		10

unsigned char ShowSensorName(unsigned char SensorID, unsigned char isTrue, float SensorVal);

#define 	ERROR_VALUE		0xEEEEEEEE

extern MAP_IN_ITEM  *map_in_list[HWL_INPUT_COUNT];	

typedef struct
{
	unsigned short int data_id;
	float value;
	void *attr_ptr;
}DATA_INFO;

unsigned char hwl_input_init(unsigned int input_ch, unsigned char Display);

unsigned int hwl_get_input_value(unsigned int input_ch, DATA_INFO *data_info);

unsigned char GetRJYSensorFlag(unsigned char *Addr);

unsigned char GetChlorophyllSensorFlag(unsigned char *Addr);

#endif



