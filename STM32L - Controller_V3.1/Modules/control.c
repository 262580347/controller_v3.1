#include "main.h"
#include "Control.h"


#define		DELAY_PLUSE()		delay_ms(80)

#define TYPE_LEVEL		(0x00)	//电平开关驱动
#define TYPE_PULSE		(0x01)	//脉冲开关驱动
#define TYPE_ROTATE_L	(0x02)	//正反停驱动（电平）
#define TYPE_ROTATE_P	(0x03)	//正反停驱动（脉冲）

#define R_STOP			(0)
#define R_POSTIVE		(1)
#define R_NEGATIVE		(2)

#define TIME_UNIT	(60)

static CHANNEL_INFO g_channel_info[OUTPUT_CHANNEL_COUNT];	//通道运行信息

static unsigned short g_nRelayState = 0;
static unsigned char s_OutputChannelCount = 0;

static unsigned char g_ucInputState[4]={0,0,0,0};

static unsigned char g_ucChannelState[4]={0,0,0,0};	//控制通道状态

unsigned char g_ArriveReportingTime = FALSE;

static void do_action(CHANNEL_INFO *channel_info);
static void OutPutChannelInfo( unsigned char index );

static unsigned char s_ControlTimeFlag = FALSE;

static unsigned char s_ReCMDFlag = FALSE;

static unsigned short s_ChannelPluseTime[OUTPUT_CHANNEL_COUNT];

void PrintOutChannelState(void);

unsigned char GetReCMDFlag(void)
{
	return s_ReCMDFlag;
}

void SetReCMDFlag(unsigned char isTrue)
{
	s_ReCMDFlag = isTrue;
}

void SetControlTimeFlag(unsigned char isTrue)
{
	s_ControlTimeFlag = isTrue;
}

typedef __packed struct
{
	unsigned char magic;
	unsigned char chksum;
	unsigned short length;
}CHANNEL_CONFIG_TAG;

UINT16 GetRelayState( void )
{
	return g_nRelayState;
}

unsigned char GetOutputChannelCount(void)
{
	return s_OutputChannelCount;
}

//系统功能: 立即启动一次状态上报动作
unsigned int system_report_status_ansync(void)
{
	g_SysCtrl.s_report_acc = g_SysCtrl.SysConfig->State_interval;
	return 1;
}

uint8 GetInputState(uint8 FeedbackChannel )
{
	uint8 ucState = 0;

	ucState = g_ucInputState[FeedbackChannel-1];

	return ucState;
}
//获取通道的运行状态
uint8 GetChannelState( uint8 ucChannelId )
{
	return( g_ucChannelState[ucChannelId] );
}



void Init_Relay_Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = CTR_R1_PIN | CTR_R2_PIN | CTR_R3_PIN | CTR_R4_PIN ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(CTR_RALAY_TYPE1_4_8, &GPIO_InitStructure);	

	CTR_R1_OFF();CTR_R2_OFF();CTR_R3_OFF();CTR_R4_OFF();
}

void Deinit_Relay_Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = CTR_R1_PIN | CTR_R2_PIN | CTR_R3_PIN | CTR_R4_PIN ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(CTR_RALAY_TYPE1_4_8, &GPIO_InitStructure);	
}

void Init_Relay_Power(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = CTR_VCC_PIN ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(CTR_VCC_TYPE, &GPIO_InitStructure);	

	CTR_VCC_ON();	
}

void Deinit_Relay_Power(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
		
	GPIO_InitStructure.GPIO_Pin = CTR_VCC_PIN ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(CTR_VCC_TYPE, &GPIO_InitStructure);	

	CTR_VCC_OFF();	
}


//某一个自控通道执行具体的远程控制指令,ChannelID：0--11
unsigned int output_manual_action(unsigned char ChannelID, unsigned char *content)
{
	CHANNEL_INFO *channel_info;
	uint8 a_id=0, d_id=0;
	uint16 duration=0;

	uint8 *ptr;

	
	if (ChannelID > OUTPUT_CHANNEL_COUNT) return 0;
	
	channel_info = (CHANNEL_INFO *)&g_channel_info[ChannelID];
	ptr = content;
	
	ptr++;

	duration = *(uint16 *)ptr;		//持续时间				
	duration = swap_word(duration);
	ptr += sizeof(uint16);

	ptr++;				//跳过特征码

	a_id = (*ptr);	//启动动作
	ptr++;

	ptr++;				//跳过特征码

	d_id = (*ptr);	//关闭动作
	
	channel_info->start_action = a_id;
	channel_info->end_action = d_id;	
	if(channel_info->start_action == 0 && duration == 1)
	{
		channel_info->duration = 0;
	}
	else
	{
		channel_info->duration = duration*60 + GetRTCSecond();
	}

	channel_info->time_acc = GetRTCSecond();	//重新计时
	channel_info->control_action = a_id;	//控制动作
	
	do_action(channel_info);   	//执行控制动作
		
	return 1;
}

//某个自控通道获取当前运行状态
unsigned int output_get_status(uint8 ChannelID, CHANNEL_STATUS *ch_status)
{
	CHANNEL_INFO *channel_info;

	if (ChannelID >= OUTPUT_CHANNEL_COUNT) return 0;
	
	channel_info = &g_channel_info[ChannelID];
	if (!channel_info->valid) return 0;

	channel_info->check_result = GetChannelState( ChannelID );	//读通道状态

	ch_status->afc_type = channel_info->afc_type;	//控制类型,默认为1
	
	switch(channel_info->ChannelConfig.ChannelType) 	//更新通道运行状态
	{
		case TYPE_LEVEL:  				//电平型
			ch_status->mark = MARK_UINT8;
			ch_status->ch_type = C_LEVEL_ID;		//类型
			//ch_status->content = (void *)&channel_info->last_action;
			ch_status->content = (void *)&channel_info->check_result;
			break;
		case TYPE_ROTATE_L:				 //电平正反停类型
			ch_status->mark = MARK_UINT8;
			ch_status->ch_type = C_RT_L_ID;
			ch_status->content = (void *)&channel_info->check_result;
			break;
		case TYPE_ROTATE_P:				  //脉冲正反停
			ch_status->mark = MARK_UINT8;
			ch_status->ch_type = C_RT_P_ID;
			ch_status->content = (void *)&channel_info->check_result;
			break;
		case TYPE_PULSE:				//脉冲开关类型
			ch_status->mark = MARK_UINT8;
			ch_status->ch_type = C_PULSE_ID;
			ch_status->content = (void *)&channel_info->check_result;
			break;
	}
	
	return 1;
}


//检测控制通道的状态
 unsigned int CheckChannelState( void )
{
	unsigned char ChannelType;
	unsigned char feedback_1, feedback_2;
	unsigned char s1, s2;
	unsigned char status = 0;
	unsigned char i=0;
	CHANNEL_INFO * channel_info = NULL;

	static unsigned char s_ucStep[4]={1,1,1,1};
	static unsigned short s_nLastTime[4]={0,0,0,0};

	for(i=0;i<OUTPUT_CHANNEL_COUNT;i++)
	{
		channel_info = (CHANNEL_INFO *)&g_channel_info[i];

		if ( channel_info->valid == 0 )
		{
			continue;
		}

		ChannelType = channel_info->ChannelConfig.ChannelType;	//设备类型

		if( channel_info->need_to_check )  	//每次有新动作后,先延时30S后更新反馈状态；
		{
		 	s_ucStep[i] = 1;
			channel_info->need_to_check = 0;
		}
		
//		//配置了反馈才进行检测
//		if( (channel_info->ChannelConfig.ChannelParam[7] == 0) && (channel_info->ChannelConfig.ChannelParam[6] == 0 ))
//		{
//			return 1;	//没有配置反馈管脚,直接返回,不检测反馈状态
//		}

		switch( s_ucStep[i] )
		{
	 	case 1:	 	//延时初始值
			s_nLastTime[i] = GetSystem100msCount();
			s_ucStep[i]++;
			g_ucChannelState[i] = channel_info->last_action;
			break;
		
		case 2:		//等待延时结束,结束后一直监测	
			switch(ChannelType)
			{
			case TYPE_LEVEL:		//电平型	
			case TYPE_PULSE:		//脉冲开关驱动
				if( CalculateTime(GetSystem100msCount(),s_nLastTime[i] ) >= 5* 10  ) 	//延时5S
				{
					//s_ucStep[i]++;
					s_nLastTime[i] += 20;	//到时间后一直检测
					feedback_1 = channel_info->ChannelConfig.ChannelParam[7];	//反馈逻辑通道配置信息
					
					//反馈通道设置无效/未设置,默认认为动作已执行成功
					if ( feedback_1 == 0 ) //根据反馈配置读管脚反馈值,返回值,0--无配置,1--配置,管脚状态：0--空闲,1--有反馈
					{
						g_ucChannelState[i] = channel_info->last_action;
					}
					else	//配置了反馈信号,使用检测到的反馈值
					{	
						s1 = GetInputState( feedback_1 );
						if( s1<2 )	//反馈状态在有效范围
						{
							g_ucChannelState[i] = s1;	//按反馈的状态为设备运行状态
						}
						else
						{
							if( channel_info->last_action == 1 )
							{
								g_ucChannelState[i] = 0xEE;	//打开故障,反馈状态为异常
							}
							else
							{
								g_ucChannelState[i] = 0xEF;//关闭故障
							}
						}
					}
				}
				break;
				
			case TYPE_ROTATE_L:		//正反停驱动(电平),目前平台上使用电平正反转的只有电动球阀,其他正反转的电气设备需要配电控箱,使用脉冲正反转类型
				if( CalculateTime(GetSystem100msCount(),s_nLastTime[i] ) >= 60* ONE_SECOND  ) 	//延时30S
				{
					s_nLastTime[i] += 50;	//到时间后一直检测
					feedback_1 = channel_info->ChannelConfig.ChannelParam[6];	//关联的输入通道,打开反馈
					feedback_2 = channel_info->ChannelConfig.ChannelParam[7];	//关联的输入通道,关闭反馈

					////根据反馈配置读管脚反馈值,返回值,0--无配置,1--配置,管脚状态：0--空闲,1--有反馈
					if ( feedback_1 == 0 || feedback_2 == 0 )
					{
						g_ucChannelState[i] = channel_info->last_action;
					}
					else	//根据反馈信号判断设备运行状态,0--无反馈,1--有反馈
					{
						s1 = GetInputState( feedback_1 );
						s2 = GetInputState( feedback_2 );
						if(s1==1 && s2==0)
						{	
							status = 1;	//正转
						}
						else if(s1==0 && s2==1)
						{	
							status = 2;	//反转
						}
						else if( s1==0 && s2 == 0 )
						{
							status = 0;	//停止或阀门不到位的情况
						}
						else
						{  	
							status = 0xFF;	//状态异常
						}
												
//						//未到位情况,电动球阀不考虑手动按钮操作
						if( status == 0 )
						{
							if( channel_info->last_action == 1 )	//打开
							{
								g_ucChannelState[i] = 0xEE;
							}
							else if( channel_info->last_action == 2 ) //关闭
							{
								g_ucChannelState[i] = 0xEF;	//没有到位信号,状态为故障
							}
							else
							{
								g_ucChannelState[i] = 0;	//停止
							}
						}
						else if ( status == 0xFF )
						{
							g_ucChannelState[i] = 0xEF;
						}
						else
						{
							g_ucChannelState[i] = status;	//按反馈状态为设备运行状态
						}
					}
				}
				break;
			
			case TYPE_ROTATE_P:		//正反转驱动(脉冲型),0--停止,1--正转,2--反转	
				if( CalculateTime(GetSystem100msCount(),s_nLastTime[i] ) >= 5* ONE_SECOND  ) 	//延时5S
				{
					s_nLastTime[i] += 50;	//到时间后一直检测
					feedback_1 = channel_info->ChannelConfig.ChannelParam[6];	//关联的输入通道,打开反馈
					feedback_2 = channel_info->ChannelConfig.ChannelParam[7];	//关联的输入通道,关闭反馈

					////根据反馈配置读管脚反馈值,返回值,0--无配置,1--配置,管脚状态：0--空闲,1--有反馈
					if ( feedback_1 == 0 || feedback_2 == 0 )
					{
						g_ucChannelState[i] = channel_info->last_action;
					}
					else	//根据反馈信号判断设备运行状态,0--无反馈,1--有反馈
					{
						s1 = GetInputState( feedback_1 );
						s2 = GetInputState( feedback_2 );
						if(s1==1 && s2==0)
						{	
							status = 1;	//正转
						}
						else if(s1==0 && s2==1)
						{	
							status = 2;	//反转
						}
						else if( s1==0 && s2 == 0 )
						{
							status = 0;	//停止
						}
						else
						{  	
							status = 0xFF;	//状态异常
						}
												
//						//异常状态
						if( status == 0xFF)
						{
							g_ucChannelState[i] = 0xEF;	//停止
						}
						else
						{
							g_ucChannelState[i] = status;	//按反馈状态为设备运行状态
						}
					}
				}
				break;
				
			default:
				g_ucChannelState[i] = channel_info->last_action;
				break;
			}
			break;
		default:
			s_ucStep[i] = 1;
			break;
		}
	}		
	return 1;
}
//检测输入状态
void CheckInputState( unsigned short nMain100ms )
{
	unsigned char i = 0;
	static uint8 s_ucFirst = 1;
	static uint16 s_nLast = 0;
	static uint8 ucState = 0;

   	if( s_ucFirst )
	{
		s_ucFirst = 0;
		s_nLast = nMain100ms;
//		FeedbackPortInit();
		return;

	}
	//FeedbackPortInit();
//	
//	ucState = GetFeedbackState();
	

	
	for(i=0;i<OUTPUT_CHANNEL_COUNT;i++)		//硬件反馈检测,0--有信号,1--无信号
	{
		if( (ucState & 0x01) == 0x01 )
			g_ucInputState[i] = 1;
		else
			g_ucInputState[i] = 0;
		ucState >>= 1;
	}
//	g_ucInputState[6] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[1], (1<<9));
//	g_ucInputState[7] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[1], (1<<8));
//	g_ucInputState[0] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<7));
//	g_ucInputState[1] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<6));
//	g_ucInputState[2] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<5));
//	g_ucInputState[3] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<4));
//	g_ucInputState[4] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<1));
//	g_ucInputState[5] = GPIOPinRead((STM32S_GPIO *)GPIO_DESC[0], (1<<0));

	//反馈接NO辅助端子,硬件电路为上拉输入,没有信号是状态是1,有信号时状态是0
	for( i=0;i<OUTPUT_CHANNEL_COUNT;i++ )
	{
		g_ucInputState[i] = 1-g_ucInputState[i];		//取反,0--空闲,1--有反馈信号
	}

	//NC反馈；0--空闲,1--有反馈信号
	
	if( CalculateTime(nMain100ms,s_nLast) >= 100 )	//10S
	{
		s_nLast = nMain100ms;
		for(i=0;i<OUTPUT_CHANNEL_COUNT;i++)
		{
//			u1_printf("[INPUT]:通道%d:(0-空闲,1-有反馈)状态=%d\r\n",i+1,g_ucInputState[i]);

		}
	}
	
	//统一使用的信号形式是：0--空闲,1--有反馈信号
}

//输出通道任务周期任务运行过程,运行间隔单位: 1秒
unsigned int output_channel_period(CHANNEL_INFO *channel_info)
{	
	RTC_TimeTypeDef RTC_TimeStructure;
	
	if ( channel_info->valid == 0 ) 
		return 0;
	
	//如果持续时间为0...
	if( channel_info->duration == 0 )
	{
		return 0;
	}

	channel_info->time_acc = GetRTCSecond();
	
	if(s_ControlTimeFlag == TRUE)
	{
		s_ControlTimeFlag = FALSE;
		u1_printf(" [Time]通道(%d),开启剩余时间:%dS\r\n", 
		channel_info->ChannelConfig.ChannelID, DifferenceOfRTCTime((channel_info->duration), GetRTCSecond()));
	}
	
	if ((channel_info->time_acc >= (channel_info->duration)))//等待串口数据处理完毕再执行控制指令
	{
		//远程控制定时到了
		//执行具体的动作关闭通道	
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);

		u1_printf("\r\n [%0.2d:%0.2d:%0.2d][控制]定时时间到, 通道:%d,执行结束动作%d ,\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, channel_info->ChannelConfig.ChannelID, channel_info->end_action);
		
		SetReCMDFlag(TRUE);
		
		Init_Relay_Power();	
//		Init_Relay_Port();
		
		delay_ms(50);
		switch(channel_info->ChannelConfig.ChannelType)
		{
			case TYPE_LEVEL:		//电平驱动
			case TYPE_ROTATE_L:		//正反停驱动(电平)
			case TYPE_ROTATE_P:		//正反停驱动(脉冲)
			case TYPE_PULSE:		//脉冲开关
				channel_info->control_action = channel_info->end_action;
				do_action(channel_info);
				break;
		}
//		Deinit_Relay_Power();
//		Deinit_Relay_Port();
		USART2_Config(LORA_BAND_RATE);
		
		delay_ms(50);
		//只执行一次
		channel_info->duration = 0;
		//通道状态改变
		channel_info->time_acc = GetRTCSecond();
		if(GetComTestFlag() == FALSE)	
		{
			g_ArriveReportingTime = TRUE;
		}
			
//		if(p_sys->Type == NETTYPE_LORA)
//		{		
//			Awake_USART2_Config();	
////			LoraPort_Init();
////			LORA_SLEEP_MODE();
//		}
//		else if(p_sys->Type == NETTYPE_SIM800C)	
//		{
//			Debug_Report_Status(SIM_COM);
//		}
//		else if(p_sys->Type == NETTYPE_GPRS)	
//		{
//			Debug_Report_Status(GPRS_COM);
//		}		

	}

	return 1;
}

void Control_Process(void)
{
	static u8 RunOnce = FALSE, s_State = 0, err = 0;
	static u16 s_LastTime = 0;
	u8 i;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		Init_Relay_Power();		//开机时给继电器电容充电,会产生瞬时电压跌落
		Init_Relay_Port();
		delay_ms(100);
		for (i=0; i<OUTPUT_CHANNEL_COUNT; i++)
		{
			if(output_channel_init(i))			//初始化所有输出通道,所有阀门关闭
			{
				s_OutputChannelCount++;
			}
			delay_ms(100);
		}
//		Deinit_Relay_Port();
//		Deinit_Relay_Power();		
		
		u1_printf("\r\n 控制器初始化完成,通道数:%d...\r\n", s_OutputChannelCount);

	}
	
	if(s_OutputChannelCount == 0)
	{
		return;
	}
		
	for(i=0;i<OUTPUT_CHANNEL_COUNT;i++)
	{
		output_channel_period(&g_channel_info[i]);		//检测是否到时关闭开关
	}
	
	CheckChannelState();			//检测输出通道状态
		
	if(g_ArriveReportingTime)			//控制时间到,状态改变,上报状态
	{	
		switch(s_State)
		{
			case 0:
				SetRecControlCmdFlag(FALSE);
//				Deinit_Relay_Port();
//				Deinit_Relay_Power();	
				delay_ms(20);
				if(p_sys->Type == NETTYPE_LORA)
				{											
					LORA_WORK_MODE();
					LORA_WORK_DELAY();	
					err = 0;					
				}			
				s_State++;
			break;
			
			case 1:
				if(p_sys->Type == NETTYPE_LORA)
				{
					Debug_Report_Status(LORA_COM);	
				}
				if(p_sys->Type == NETTYPE_SIM800C)	
				{
					Debug_Report_Status(SIM_COM);
				}
				else if(p_sys->Type == NETTYPE_GPRS)	
				{
					Debug_Report_Status(GPRS_COM);
				}
				s_LastTime = GetSystem100msCount();
				s_State++;
			break;
			
			case 2:
				if(g_RecAck)
				{
					PrintOutChannelState();
					if(GetRecControlCmdFlag() == FALSE)	//上报的间隙又接收到控制指令，将重新上报状态
					{									
						if(p_sys->Type == NETTYPE_LORA)
						{		
							SetReCMDFlag(FALSE);
						}
						g_ArriveReportingTime = FALSE;	
					}
					g_RecAck = FALSE;
					s_State = 0;
					err = 0;
					s_LastTime = GetSystem100msCount();
				}
				else if(CalculateTime(GetSystem100msCount(), s_LastTime) >= 20)	
				{
					LORA_WORK_MODE();
					err++;
					s_State--;
					u1_printf("\r\n --重发--\r\n");
					if(err >= 3)
					{
						u1_printf(" 超时\r\n");
						PrintOutChannelState();
						if(p_sys->Type == NETTYPE_LORA)
						{		
							LORA_LOWPOWER_MODE();
							SetReCMDFlag(FALSE);
						}		
						SetReCMDFlag(FALSE);
						g_ArriveReportingTime = FALSE;			
						err = 0;
						s_State = 0;
						s_LastTime = GetSystem100msCount();
					}
				}
			break;							
		}		
	}
}

MAP_OUT_ITEM * get_channel_config_item( unsigned int index )
{
	MAP_OUT_ITEM *item;
	
	item = NULL;
	if (index >= OUTPUT_CHANNEL_COUNT) return item;
	
	item = (MAP_OUT_ITEM *)&g_channel_info[index].ChannelConfig;
	return item;
}


//加载对应通道的设备配置数据,获取参数信息指针结构体,方便使用,返回值0--配置有效,1--配置无效
unsigned int load_channel_config(uint16 index)
{
	MAP_OUT_ITEM * ChannelConfig;	//变量
	
	ChannelConfig = (MAP_OUT_ITEM *)&g_channel_info[index].ChannelConfig;
	ChannelConfig->ChannelID = index+1;
	
	if( GetChannelConfigInfo(index,(unsigned char *)ChannelConfig) == 0 )
	{
	//	u1_printf("[配置]:读取通道%02d配置信息失败\r\n", index+1);

		return 0;
	}		
	
	return 1;
}

//如果有电平开关处于开启状态 返回1，否则返回0
unsigned char GetLeveloutputStatus(void)
{
	unsigned char ChannelCount = 0, i, j = 0;
	unsigned char LevelOutputStatus[4] = {0, 0, 0, 0};
	unsigned char OutputType[4] = {0, 0, 0, 0};
	ChannelCount = GetOutputChannelCount();

	for(i=0; i<OUTPUT_CHANNEL_COUNT; i++)
	{
		for(; j<OUTPUT_CHANNEL_COUNT; j++)
		{
			if(g_channel_info[j].valid)
			{		
				
				if(g_channel_info[i].ChannelConfig.ChannelType == TYPE_LEVEL)
				{
					OutputType[i] = 1;
				}
							
				if(g_ucChannelState[j] == 1)
				{
					LevelOutputStatus[i] = 1;						
				}
				if((LevelOutputStatus[i] & OutputType[i]) > 0)
				{
					return 1;
				}
				
				j++;
				break;
			}	
		}		
	}	

	return 0;	
}

void PrintOutChannelState(void)
{
	unsigned char ChannelCount = 0, i, j = 0;
	
	ChannelCount = GetOutputChannelCount();
	u1_printf("\r\n Channel State(%d):\r\n", ChannelCount);
	for(i=0; i<OUTPUT_CHANNEL_COUNT; i++)
	{
		for(; j<OUTPUT_CHANNEL_COUNT; j++)
		{
			if(g_channel_info[j].valid)
			{		
				u1_printf(" %d. ", g_channel_info[j].ChannelID);
				
				switch(g_channel_info[i].ChannelConfig.ChannelType)
				{
					case TYPE_LEVEL:		//电平驱动
						u1_printf(" 电平开关:");
					break;
					
					case TYPE_ROTATE_L:		//正反停驱动(电平)
						u1_printf(" 电平正反:");
					break;
					
					case TYPE_ROTATE_P:		//正反停驱动(脉冲)
						u1_printf(" 脉冲正反:");
					break;
					
					case TYPE_PULSE:		//脉冲开关
						u1_printf(" 脉冲开关:");
					break;					
				}
				
				switch(g_ucChannelState[j])
				{
					case 0:
						u1_printf("停止\r\n");
					break;
					
					case 1:
						u1_printf("开启\r\n");
					break;
					
					case 2:
						u1_printf("关闭\r\n");
					break;
					
					case 0xEE:
						u1_printf("打开故障\r\n");
					break;
					
					case 0xEF:
						u1_printf("关闭故障\r\n");
					break;
				}
				j++;
				break;
			}	
		}		
	}	
}
//串口输出通道的配置信息
static void OutPutChannelInfo( unsigned char index )
{		

	unsigned short PluseDelayTime = 0;
	
	u1_printf("\r\n 通道：%d, ", g_channel_info[index].ChannelConfig.ChannelID);
	
	switch(g_channel_info[index].ChannelConfig.ChannelType)
	{
		case 0:
			u1_printf("电平驱动  输出:%d\r\n", g_channel_info[index].ChannelConfig.ChannelParam[0]);
		break;
		
		case 1:
			u1_printf("脉冲输出  关:%d 开:%d\r\n", g_channel_info[index].ChannelConfig.ChannelParam[0], g_channel_info[index].ChannelConfig.ChannelParam[1]);
		break;
		
		case 2:
			u1_printf("电平型正反转  正转:%d 反转:%d\r\n", g_channel_info[index].ChannelConfig.ChannelParam[0], g_channel_info[index].ChannelConfig.ChannelParam[1]);
		break;
		
		case 3:
			u1_printf("脉冲型正反转  正:%d 反:%d 停:%d\r\n", g_channel_info[index].ChannelConfig.ChannelParam[0], g_channel_info[index].ChannelConfig.ChannelParam[1], g_channel_info[index].ChannelConfig.ChannelParam[2]);
		break;
	}
	
	if(g_channel_info[index].ChannelConfig.ChannelParam[4])
	{
		u1_printf(" 反馈通道:(%d)(%d)\r\n", g_channel_info[index].ChannelConfig.ChannelParam[4], g_channel_info[index].ChannelConfig.ChannelParam[5]);
	}
	
	if(g_channel_info[index].ChannelConfig.ChannelReverse[0])
	{
		u1_printf(" 关联传感器通道:(%d)\r\n", g_channel_info[index].ChannelConfig.ChannelReverse[0]);
	}
	
	if(g_channel_info[index].ChannelConfig.ChannelReverse[1] || g_channel_info[index].ChannelConfig.ChannelReverse[2])
	{
		PluseDelayTime = (g_channel_info[index].ChannelConfig.ChannelReverse[1]) + (g_channel_info[index].ChannelConfig.ChannelReverse[2] << 8);
		u1_printf(" 脉冲时间:(%dms)\r\n", PluseDelayTime);
		s_ChannelPluseTime[index] = PluseDelayTime;
	}
	return;
}

void RelayControl( unsigned char ucChannel, unsigned char ucState )
{
	if( ucState )
	{
		g_nRelayState |= (0x0001<<(ucChannel-1));
		
		switch(ucChannel)
		{
			case 1:
				CTR_R1_ON();
			break;
			
			case 2:
				CTR_R2_ON();
			break;
					
			case 3:
				CTR_R3_ON();
			break;
			
			case 4:
				CTR_R4_ON();
			break;
			
		}
	}
	else
	{
		g_nRelayState &= ~(0x0001<<(ucChannel-1));
		switch(ucChannel)
		{
			case 1:
				CTR_R1_OFF();
			break;
			
			case 2:
				CTR_R2_OFF();
			break;
					
			case 3:
				CTR_R3_OFF();
			break;
			
			case 4:
				CTR_R4_OFF();
			break;
			
		}
	}
	

}
//电平控制  output_ch:1--12
unsigned int hwl_drv_level(unsigned int output_ch, unsigned int value)
{
	unsigned char ucChannel;

	ucChannel = output_ch;


	
	if( ucChannel > OUTPUT_CHANNEL_COUNT*2 ) 
	{
		u1_printf("ucChannel%d > OUTPUT_CHANNEL_COUNT\r\n", ucChannel);
		return 0;
	}
	
	if( value )
	{
		RelayControl( ucChannel,1 );	//吸合控制继电器
//		SetRelayLedState( ucChannel-1,1);
//		UpdateLedState( GetLedState() );
	}
	else
	{
		RelayControl( ucChannel,0 );	//释放控制继电器
//		SetRelayLedState( ucChannel-1,0);
//		UpdateLedState( GetLedState() );
	}
	
//	u1_printf("[电平控制]通道%d,动作=%d,g_nRelayState=0x%04X\r\n",ucChannel,(uint16)value,GetRelayState() );	
	return 1;
}
static void PluseDelay(unsigned short DelayTime)
{
	unsigned short Count = 0, Remainder = 0, i;
	
	
	Count = DelayTime/1000;
	Remainder = DelayTime%1000;
	
	if(Count)
	{
		for(i=0; i<Count; i++)
		{
			delay_ms(1000);
		}
		IWDG_ReloadCounter();
	}
	if(Remainder)
	{
		delay_ms(Remainder);
	}
}
//脉冲控制
unsigned int hwl_drv_rotate_pulse(unsigned char Channel, unsigned int output_ch)
{
	unsigned char ucChannel;
	
	ucChannel = output_ch;
	if( ucChannel > OUTPUT_CHANNEL_COUNT*2 )
	{
		u1_printf("ucChannel:%d > OUTPUT_CHANNEL_COUNT pluse\r\n", ucChannel);
		return 0;
	}

	RelayControl( ucChannel,1 );	//吸合控制继电器

	if(s_ChannelPluseTime[Channel-1])
	{
		PluseDelay(s_ChannelPluseTime[Channel-1]);
	}
	else
	{
		DELAY_PLUSE();
	}
	
	RelayControl( ucChannel,0 );	//释放控制继电器
	
	delay_ms(40);
	
	return 1;
}

//正反转电平型
unsigned int hwl_drv_rotate_level(unsigned int output_ch, unsigned int value)
{
	unsigned char ucChannel;

	ucChannel = output_ch;
	if( ucChannel > OUTPUT_CHANNEL_COUNT*2 ) return 0;
	
//	u1_printf("[电平正反转]通道%d,动作=%d,g_nRelayState=0x%04X\r\n",ucChannel,(uint16)value,GetRelayState());

	if( value )
	{
		RelayControl( ucChannel,1 );	//吸合控制继电器
//		SetRelayLedState( ucChannel-1,1);
//		UpdateLedState( GetLedState() );
	}
	else
	{
		RelayControl( ucChannel,0 );	//释放控制继电器
//		SetRelayLedState( ucChannel-1,0);
//		UpdateLedState( GetLedState() );
	}
	
	return 1;
}
//执行自控通道设定的具体输出动作(电平驱动&脉冲驱动&正反转etc.)
static void do_action(CHANNEL_INFO *channel_info)
{
	unsigned char output_id1,output_id2,output_id3;

	switch( channel_info->ChannelConfig.ChannelType)
	{
		case TYPE_LEVEL: 	//电平型控制动作
			output_id1 = channel_info->ChannelConfig.ChannelParam[0];	//管脚配置
			channel_info->last_action = channel_info->control_action;
			if ( channel_info->control_action == 1 )
			{
				hwl_drv_level(output_id1, 1);	//打开,操作硬件端口
			}
			else if ( channel_info->control_action == 0 )
			{
				hwl_drv_level(output_id1, 0);	//关闭,操作硬件端口
			}
			else
			{
				u1_printf(" error cmd\r\n");
			}
			break;
		case TYPE_PULSE: 	//脉冲开关控制动作
			output_id1 = channel_info->ChannelConfig.ChannelParam[0];	//打开管脚配置
			output_id2 = channel_info->ChannelConfig.ChannelParam[1];	//关闭管脚配置
			channel_info->last_action = channel_info->control_action;
			switch (channel_info->control_action)
			{
				case 0:	//打开
			//		hwl_drv_level(output_id2, 0);
					hwl_drv_rotate_pulse(channel_info->ChannelID, output_id1);	//正脉冲
					break;
				case 1:	//关闭
			//		hwl_drv_level(output_id1, 0);
					hwl_drv_rotate_pulse(channel_info->ChannelID, output_id2);	//负脉冲
					break;
			}
			break;
		case TYPE_ROTATE_L:	//电平正反转
			output_id1 = channel_info->ChannelConfig.ChannelParam[0];	  //正转管脚配置
			output_id2 = channel_info->ChannelConfig.ChannelParam[1];	  //反转管脚配置
			//output_id3 = channel_info->ChannelConfig.ChannelParam[2];	//停止管脚配置
			channel_info->last_action = channel_info->control_action;
			
			switch(channel_info->control_action)
			{
				case R_STOP:
					hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 0);
					//hwl_drv_level(output_id1, 1);
					break;
				case R_POSTIVE:
					//hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 0);
					hwl_drv_level(output_id1, 1);
					break;
				case R_NEGATIVE:
					//hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 1);
					break;
			}
			break;
		case TYPE_ROTATE_P:	 //脉冲正反转
			output_id1 = channel_info->ChannelConfig.ChannelParam[0];	 	//正转
			output_id2 = channel_info->ChannelConfig.ChannelParam[1];		//反转
			output_id3 = channel_info->ChannelConfig.ChannelParam[2];		//停止
			
		
			//默认执行动作指令前,先复位到停止模式
			if( channel_info->control_action != channel_info->last_action )
			{
				hwl_drv_level(output_id1, 0);
				hwl_drv_level(output_id2, 0);
				hwl_drv_rotate_pulse(channel_info->ChannelID, output_id3);		//先执行一次停止的动作
			}
			
			channel_info->last_action = channel_info->control_action;
			
			switch(channel_info->control_action)
			{	
				case R_STOP:
					hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 0);
					hwl_drv_rotate_pulse(channel_info->ChannelID, output_id3);	//停止管脚发一个50mS脉冲
					break;
				case R_POSTIVE:
					hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 0);
					hwl_drv_rotate_pulse(channel_info->ChannelID, output_id1);	//正转管脚发一个50mS脉冲
					break;
				case R_NEGATIVE:
					hwl_drv_level(output_id1, 0);
					hwl_drv_level(output_id2, 0);
					hwl_drv_rotate_pulse(channel_info->ChannelID, output_id2);	//反转管脚发一个50mS脉冲
					break;
			}
			break;
	}

	//执行动作后,立刻上报状态,并启动检查
	channel_info->need_to_check = 1;
	system_report_status_ansync();	   	//重置上报设备状态时间计数为立即上报设备状态
	
	return;
}

//输出通道任务初始化
unsigned int output_channel_init(unsigned char index)
{
	if (index > OUTPUT_CHANNEL_COUNT) return 0;
	
	memset(&g_channel_info[index], 0, sizeof(CHANNEL_INFO));	//清0
//	g_channel_info[ChannelID].param_info = &g_param_info[ChannelID];	//通道的配置信息指向配置数据g_param_info[ChannelID]
	
	g_channel_info[index].ChannelID = index+1;	//通道号
	
	g_channel_info[index].valid = load_channel_config(index);	//读输出配置参数
	
	g_channel_info[index].afc_type = 1;		//本地远程控制：0--本地,1--远程
	g_channel_info[index].time_acc = 0;		//计算累计值
	g_channel_info[index].control_action = 0;	//控制执行动作
	g_channel_info[index].start_action = 0;		//起始动作
	g_channel_info[index].end_action = 0;		//结束动作
	g_channel_info[index].duration = 0;			//持续时间
	g_channel_info[index].last_action = 0;		//最后新执行的动作
	g_channel_info[index].need_to_check = 0;	//是否需要检测反馈
	g_channel_info[index].check_result = 0;		//检测反馈结果
	
	if ( g_channel_info[index].valid == 1 )	//参数是否有效
	{
		OutPutChannelInfo(index);
		g_channel_info[index].control_action = 0;
		do_action(&g_channel_info[index]);		//上电时执行一次动作初始化设备状态	
	}
	else
	{
	//	u1_printf("[初始化]：通道 %d 参数未配置.\r\n", index+1);
		return 0;
	}
	
	return 1;
}

/////查询输出状态-0x0002 ======================================================
//static unsigned int Report_Control_State(void)
//{
//	unsigned char send_buf[128], Packet[128];
//	unsigned int send_len = 0, nMsgLen;
//	unsigned int payload_len;
//	static u16 seq = 0;
//	CLOUD_HDR *hdr;				//数据包头格式
//	unsigned char *ptr;
//	unsigned char count;
//	CHANNEL_STATUS ch_status;	 //通道状态
//	unsigned short ch,val_u16;
//	int i;
//	SYSTEMCONFIG *p_sys;
//	
//	p_sys = GetSystemConfig();	
//	
//	hdr = (CLOUD_HDR *)send_buf;
//	hdr->protocol = swap_word(PROTOCOL_CONTROL_ID);			//通信版本号
//	hdr->device_id = swap_dword(p_sys->Device_ID);	  //设备号
//	hdr->dir = 0;												  //方向
//	hdr->seq_no = swap_word(seq);							  //包序号
//	seq++;
//	hdr->cmd = 0x02;									  //命令字										  //方向
//	
//	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
//	ptr++;

//	count = 0;
//	for (i=0;i<OUTPUT_CHANNEL_COUNT;i++)
//	{
//		if (output_get_status(i, &ch_status))  	//读取输出通道的运行状态
//		{
//			ch = i+32+1;
//			val_u16 = swap_word(ch);
//			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
//			ptr += sizeof(unsigned short);
//			val_u16 = swap_word(ch_status.ch_type);
//			memcpy(ptr, (void *)&val_u16, sizeof(unsigned short));
//			ptr += sizeof(unsigned short);
//			(*ptr) = ch_status.afc_type;
//			ptr++;
//			(*ptr) = ch_status.is_manual;
//			ptr++;
//			(*ptr) = ch_status.mark;
//			ptr++;
//			(*ptr) = *((unsigned char *)ch_status.content);
//			ptr++;
//			count++;
//		}
//	}
//	
//	send_buf[sizeof(CLOUD_HDR)] = count;
//	send_len = ptr - send_buf;
//    payload_len = send_len - sizeof(CLOUD_HDR);
//	hdr->payload_len = swap_word(payload_len);
//	
//	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
//	send_len++;
//	
//	nMsgLen = PackMsg( send_buf,send_len,Packet,120);

//	Lora_Send_Data(p_sys->Net, p_sys->Channel, Packet, nMsgLen);
//	
//	return 1;	
//}
