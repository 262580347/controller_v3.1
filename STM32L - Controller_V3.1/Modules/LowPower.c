/**********************************
说明：低功耗模式下的进入与退出管理
	  主要重新对时钟、外设、端口进行配置
	  清除对应标志位
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "main.h"
#include "LowPower.h"
 
static unsigned char s_RTCAlarmFlag = FALSE, s_Exti2Flag = FALSE;

static unsigned char s_EnterStopFlag = FALSE, s_StopTime = 0;

unsigned char GetStopModeFlag(void)
{
	return s_EnterStopFlag;
}

void SetStopModeFlag(unsigned char isTrue)
{
	s_EnterStopFlag = isTrue;
}

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime)
{
	s_EnterStopFlag = isTrue;
	s_StopTime = StopTime;
}

void StartGetBatVol(void)
{
	g_PowerDetectFlag = FALSE;
}

void StopModeProcess(void)
{
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	if(GetStopModeFlag() && g_PowerDetectFlag)
	{
		if(p_sys->Type == NETTYPE_LORA)
		{
			EnterStopMode(s_StopTime);
		}
		else
		{
			EnterStopMode_SIM(s_StopTime);
		}
	}
}

//进入停止模式前的配置,GPIO、中断、外设,使用快速唤醒模式

void EnterStopMode(unsigned char Second)	
{  	

	
	if(Second == 0)
	{
		return;
	}	

	if(GetLeveloutputStatus() == 0)
	{
		u1_printf("没有电平开关开启\r\n");
		Deinit_Relay_Power();
	}
	else
	{
		u1_printf("有电平开关开启\r\n");
	}
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 	//关闭TIM4
		
	__disable_irq();		//关闭全局中断
	
	g_RecAck = FALSE;		//清接收应答标志

	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 			//ADC设为默认值
				
	RTC_SetAlarmA(Second);		//设置闹钟唤醒时间

	if(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == RESET)
	{
		u1_printf(" Wait Aux...  ");
		while(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == RESET);
		delay_ms(1);
	}
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);

	DMA_Cmd(DMA1_Channel4, DISABLE); 
	DMA_DeInit(DMA1_Channel4);

	DMA_Cmd(DMA1_Channel2, DISABLE); 
	DMA_DeInit(DMA1_Channel2);
	
	SetLEDFlag(FALSE);
	Stop_GPIO_Init();	

	EXTI_Aux_Config();	//设置AUX为外部唤醒口
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
//	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	


}

void EnterStopMode_SIM(unsigned char Second)	
{  	
	
	if(Second == 0)
	{
		return;
	}	

	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 	
	
	USART_Cmd(USART2,DISABLE); 
	USART_DeInit(USART2);	
	__disable_irq();
//	delay_ms(3);
	
	SIM_PWR_OFF();
	
//	u1_printf(" Stop\r\n");
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 	
	
	RTC_SetAlarmA(Second);
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);

	DMA_Cmd(DMA1_Channel4, DISABLE ); 
	DMA_DeInit(DMA1_Channel4);
	
	Stop_GPIO_Init_SIM();		
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	
}


void EnterStandbyMode(void)
{
	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in standby mode after WFI instruction and not in stop */
	PWR->CR |= PWR_CR_PDDS;

	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);

	/* Wait for interrupt instruction, device go to sleep mode */
	__WFI();	

}


void LowPower_Process(void)
{
	static unsigned int n_OverTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	

	if(s_RTCAlarmFlag)			//闹钟唤醒
	{
		s_RTCAlarmFlag = FALSE;
				
		Wake_SysInit();		//GPIO配置
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
		u1_printf("\r\n %02d:%02d:%02d ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
//		
		EXTI_Aux_Reset();	//关闭AUX唤醒
		
		EXTI17_Reset();		//复位闹钟中断
		
		if(DifferenceOfRTCTime(GetRTCSecond(), n_OverTime) >= 60)
		{
			n_OverTime = GetRTCSecond();
			
			StartGetBatVol();	
		}
	}
	
	if(s_Exti2Flag)				//外部唤醒
	{			
		s_Exti2Flag = FALSE;
				
		Wake_SysInit();		//GPIO配置
		
//		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
//		u1_printf("\r\n [%02d:%02d:%02d]  Aux\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

		EXTI_Aux_Reset();		//关闭AUX唤醒
		
		EXTI17_Reset();		//复位闹钟中断
		
	}	
}
void RTC_Alarm_IRQHandler(void)	//闹钟中断
{
	if(RTC_GetFlagStatus(RTC_FLAG_ALRAF) != RESET)
	{		
		SysClockForMSI(RCC_MSIRange_6);	//内部时钟
		
//		Awake_USART3_Config();		//LORA串口配置,接收串口信息		
//		USART2_Config(LORA_BAND_RATE);
		
//		USART1_Config(115200);	
//		
//		u1_printf(" RTC_Alarm_IRQHandler...\r\n");
		
		IWDG_ReloadCounter();
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)	//清PWR_FLAG_WU
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		EXTI_ClearITPendingBit(EXTI_Line17);
		
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		
		s_RTCAlarmFlag = TRUE;
		
		if(s_EnterStopFlag)
		{
			s_EnterStopFlag = FALSE;
		}
	}	
}


void EXTI15_10_IRQHandler(void)	//外部中断,LORA唤醒包
{	
	if(EXTI_GetITStatus(EXIT_LINE_PIN) != RESET)
	{			
		SysClockForMSI(RCC_MSIRange_6);	//内部时钟

		USART2_Config(LORA_BAND_RATE);
		
		IWDG_ReloadCounter();
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)	//清PWR_FLAG_WU
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		SetReCMDFlag(TRUE);
		
		s_Exti2Flag = TRUE;
				
		if(s_EnterStopFlag)
		{
			s_EnterStopFlag = FALSE;
		}
		
		EXTI_ClearITPendingBit(EXIT_LINE_PIN);
	}
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

