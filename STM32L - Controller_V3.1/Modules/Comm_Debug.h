#ifndef		_COMM_DEBUG_H_
#define		_COMM_DEBUG_H_
//#include "sys.h"

#define	CMD_TEST_MODE		(0xAA)
#define	CMD_RESET			(0xAB)

#define CMD_GET_DATETIME 	(0x00F1)	//读取设备时间
#define CMD_SET_DATETIME	(0x00F2)	//设置设备时间
#define CMD_RD_SYS_INFO  	(0x00F3)	//读取系统信息
#define CMD_WR_SYS_INFO  	(0x00F4)	//设置系统信息
#define CMD_RD_SYS_CFG   	(0x00F5)	//读取通信配置
#define CMD_WR_SYS_CFG   	(0x00F6)	//设置通信配置

#define CMD_GET_OUT_STATE	(0x0002)	//查询输出控制状态

#define CMD_MANUAL_CTRL  	(0x0003) 	//手动控制

#define CMD_INPUT_COUNT		(0x0010)	//获取设备额定单元输入参量个数
#define CMD_GET_INPUT		(0x0011)	//读取一个单元输入参量配置信息
#define CMD_SET_INPUT		(0x0012)	//设置一个单元输入参量配置信息
#define CMD_DEL_INPUT		(0x00F7)	//删除一个输入配置

#define CMD_OUTPUT_COUNT	(0x0013)	//获取设备额定单元输出通道个数
#define CMD_GET_OUTPUT		(0x0014)	//读取一个单元输出通道配置信息
#define CMD_SET_OUTPUT		(0x0015)	//设置一个单元输出通道配置信息
#define CMD_DEL_OUTPUT		(0x00F8)	//删除一个输出配置

//数据存储功能调试测试指令
#define CMD_INIT_LOG		(0x00D0)
#define CMD_ERASE			(0x00D1)
#define CMD_GEN_LOG			(0x00D2)
#define CMD_LOG_INFO		(0x00D3)
#define CMD_RD_A_LOG		(0x00D4)
#define CMD_RD_FLASH		(0x00D5)
#define CMD_WR_FLASH		(0x00D6)
	

void OnDebugComm(unsigned char CommData);
void TaskForComObser(void);	
void OnDebug(unsigned char *data, unsigned char lenth);
#endif
