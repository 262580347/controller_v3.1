/**********************************
说明：慧云配置工具串口通信程序
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "Comm_Debug.h"
#include "main.h"


static u16 PackMsg(u8* srcbuf,u16 srclen,u8* destbuf,u16 destsize)
{
	u16 i,j;

	j = 0;
	i = 0;
	destbuf[j++] = BOF_VAL;
	for (i=0;i<srclen;i++)
	{
		switch (srcbuf[i])
		{
		case 0x7d:
			destbuf[j++]=0x7d;
			destbuf[j++]=0x5d;
			break;		
		case 0x7e:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x5e;
			break;
		case 0x21:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x51;
			break;
		default:
			destbuf[j++]=srcbuf[i];
			break;
		}
		if ( j > destsize- 2) break;
	}

	destbuf[j++] = EOF_VAL;

	return j;
}

static void Debug_Send_Packet(u8 *packet, u8 len)
{
	u8 send_buf[256];
	u8 send_len;
	
	send_len = PackMsg(packet, len, send_buf, 256);			//数据包转义处理					
	Uart_Send_Data(USART1, send_buf, send_len);	
}

static void Debug_Get_Time(u8 *data, u8 lenth)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u8 *p_data;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(6);
	p_data = &send_buf[sizeof(CLOUD_HDR)];
	
	*(p_data++) = RTC_TimeStructure.RTC_Seconds;
	*(p_data++) = RTC_TimeStructure.RTC_Minutes; 
	*(p_data++) = RTC_TimeStructure.RTC_Hours;

	*(p_data++) = RTC_DateStruct.RTC_Date;
	*(p_data++) = RTC_DateStruct.RTC_Month;
	*(p_data++) = (u8)(RTC_DateStruct.RTC_Year);
	
	send_len += 6;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;

	Debug_Send_Packet(send_buf, send_len);
	
}	

static void Debug_Updata_Time(u8 *data, u8 lenth)
{
	u8 *p_data;
	u8 send_buf[64];
	u32 send_len;
	CLOUD_HDR *hdr;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
		
	p_data = (u8 *)&data[sizeof(CLOUD_HDR)];
	
	RTC_TimeStructure.RTC_Seconds	 = *(p_data++);
	RTC_TimeStructure.RTC_Minutes 	 = *(p_data++); 
	RTC_TimeStructure.RTC_Hours	 	 = *(p_data++);
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

	RTC_DateStruct.RTC_Date  	= *(p_data++);
	RTC_DateStruct.RTC_Month  	= *(p_data++);
	RTC_DateStruct.RTC_Year  	= *(p_data++);	
	RTC_DateStruct.RTC_WeekDay  = 1;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
	
	RTC_WaitForSynchro();//等待RTC寄存器同步   
		
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	RTC_TimeShow();
}

static void Debug_Read_SystemInfo(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEM_INFO 	*sys_info;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEM_INFO));
	sys_info = Get_SystemInfo();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_info, sizeof(SYSTEM_INFO));
	send_len += sizeof(SYSTEM_INFO);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}


static void Debug_Read_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG));
	sys_cfg = GetSystemConfig();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_cfg, sizeof(SYSTEMCONFIG));
	send_len += sizeof(SYSTEMCONFIG);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

static unsigned char Debug_Set_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[64], ReturnVal = 0;
	u32 send_len;
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	
	sys_cfg = (SYSTEMCONFIG *)&data[sizeof(CLOUD_HDR)];
	ReturnVal = Set_System_Config(sys_cfg);
	if(ReturnVal == FALSE)
	{
		return FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return TRUE;
}


static u32 Debug_Input_Count(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u32 count;
	u8 send_buf[64];
	u32 send_len;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	count = 0;
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &count, sizeof(u32));
	send_len += sizeof(u32);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

//读取一个单元输入参量配置信息 --0x11
static u32 Debug_Get_Input(u8 *data, u32 len)
{
	u8 send_buf[128];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	MAP_IN_ITEM *item;
	u16 input_ch;
	
	input_ch = *(uint16 *)&data[sizeof(CLOUD_HDR)];		
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	item = get_map_in_item(input_ch);
	if (item)
	{
		hdr->payload_len = swap_word(sizeof(u16)+sizeof(MAP_IN_ITEM)+item->spec_len);
		memcpy(&send_buf[send_len], item, sizeof(MAP_IN_ITEM)+item->spec_len);
		send_len += sizeof(MAP_IN_ITEM)+item->spec_len;
	}
	else
	{
		hdr->payload_len = swap_word(sizeof(u16));
	}
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);
	
	return 1;
}

//设置一个单元输入参量配置信息 --0x12
static u32 Debug_Set_Input(u8 *data, u32 len)
{
	u8 send_buf[128];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	MAP_IN_ITEM *item;
	u16 input_ch;
	u8 ack;
	
	input_ch = *(uint16 *)&data[sizeof(CLOUD_HDR)];		
	item = (MAP_IN_ITEM *)&data[sizeof(CLOUD_HDR)+sizeof(u16)];	//配置数据
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u16)+1);
	
	ack = set_map_in_item(input_ch, item);	   				//设置结果
	hwl_input_init(input_ch, FALSE);
	
	send_buf[send_len] = ack;
	send_len++;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	return 1;
}

//删除一个输入配置 --0xF7 ===========================================
static u32 Debug_Delete_Input(u8 *data, u32 len)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u16 channel;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
	
	channel = *(u16 *)&data[sizeof(CLOUD_HDR)];
	
	//删除一个输入配置信息
	set_map_in_item(channel, NULL);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	return 1;
}

static u32 Debug_Output_Count(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u32 count;
	u8 send_buf[64];
	u32 send_len;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	count = 4;
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &count, sizeof(u32));
	send_len += sizeof(u32);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

//读取一个单元输出通道配置信息--0x14
static unsigned int Get_OutputChannel(u8 *data, u32 len)
{
	unsigned char send_buf[128];
	unsigned int send_len;
	
	CLOUD_HDR *hdr;
	MAP_OUT_ITEM *item;
	unsigned short output_ch;
	
	output_ch = *(unsigned short *)&data[sizeof(CLOUD_HDR)];  //通道号2字节
	
	//以下为MAP_OUT_ITEM信息
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(unsigned short));
	send_len = sizeof(CLOUD_HDR)+sizeof(unsigned short);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	item = get_channel_config_item(output_ch-1);	//指向内存区域,g_channel_info[index].ChannelConfig;
	if (item)
	{
		hdr->payload_len = swap_word(sizeof(unsigned short)+sizeof(MAP_OUT_ITEM) );
		memcpy(&send_buf[send_len], (unsigned char *)item, sizeof(MAP_OUT_ITEM) );
		send_len += sizeof(MAP_OUT_ITEM);
	}
	else
	{
		hdr->payload_len = swap_word(sizeof(unsigned short));
	}
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

//设置一个单元输出通道关联管脚配置信息--0x15
static unsigned int Set_Output_Channel(u8 *data, u32 len)
{
	unsigned char send_buf[64];
	unsigned int send_len;
	
	CLOUD_HDR *hdr;
	MAP_OUT_ITEM *item;
	unsigned short output_ch;
	unsigned char ack;
	
	output_ch = *(unsigned short *)&data[sizeof(CLOUD_HDR)];		//通道号
	item = (MAP_OUT_ITEM *)&data[sizeof(CLOUD_HDR)+sizeof(unsigned short)];	//MAP_OUT_ITEM内容
	
//	u1_printf("收到通道%d配置:通道=%02d,类型=%d,控制端口=%d,%d,%d,%d,反馈端口=%d,%d,%d,%d\r\n"
//				, item->ChannelID
//				, item->ChannelID
//				, item->ChannelType
//				, item->ChannelParam[0]
//				, item->ChannelParam[1]
//				, item->ChannelParam[2]
//				, item->ChannelParam[3]
//				, item->ChannelParam[4]
//				, item->ChannelParam[5]
//				, item->ChannelParam[6]
//				, item->ChannelParam[7]);


	ack = SetChannelConfig(output_ch -1 , item);	//设置输出通道参数		
	
	output_channel_init( output_ch - 1 );		//重新加载改通道参数
	
//	u1_printf("设置后通道%d配置:通道=%02d,类型=%d,控制端口=%d,%d,%d,%d,反馈端口=%d,%d,%d,%d\r\n"
//				, item->ChannelID
//				, item->ChannelID
//				, item->ChannelType
//				, item->ChannelParam[0]
//				, item->ChannelParam[1]
//				, item->ChannelParam[2]
//				, item->ChannelParam[3]
//				, item->ChannelParam[4]
//				, item->ChannelParam[5]
//				, item->ChannelParam[6]
//				, item->ChannelParam[7]);

	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(unsigned short));	//协议头 + 通道号
	send_len = sizeof(CLOUD_HDR)+sizeof(unsigned short);	//长度 通道号
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(unsigned short)+1);	//长度3
	
	send_buf[send_len] = ack;
	send_len++;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

//删除一个输出管脚配置  --0xF8
static unsigned int Delete_Output_Channel(u8 *data, u32 len)
{
	unsigned char send_buf[64];
	unsigned int send_len;
	
	CLOUD_HDR *hdr;
	unsigned short channel;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(unsigned short));
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	send_len = sizeof(CLOUD_HDR)+sizeof(unsigned short);
	
	channel = *(unsigned short *)&data[sizeof(CLOUD_HDR)];
	
	//删除一个输出配置信息
	//set_map_out_item(channel, NULL);
	//hwl_output_init(channel);	
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	//先发生响应命令再执行操作,节省通信时间
	SetChannelConfig( channel,NULL);
	//load_channel_config(channel);
	output_channel_init( channel );
	
	return 1;
}

static void Debug_Enter_Test_Mode(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u8 send_buf[64];
	u32 send_len;
	
	if(GetComTestFlag() == FALSE)
	{
		SetComTestFlag(TRUE);
	}
	else
	{
		SetComTestFlag(FALSE);
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

void OnDebug(u8 *data, u8 lenth)
{
	u8 ReturnVal = 0;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)data;
	switch(hdr->cmd)
	{
		case CMD_GET_DATETIME:	//获取设备时间	--0xF1
			Debug_Get_Time(data, lenth);
			break;
		case CMD_SET_DATETIME:	 //设置设备时间 --0xF2
			Debug_Updata_Time(data, lenth);
			break;
		
		case CMD_RD_SYS_INFO:	//读取系统信息 --0xF3
			Debug_Read_SystemInfo(data, lenth);
			break;
	
		case CMD_RD_SYS_CFG:	//读取系统配置 --0xF5
			Debug_Read_SystemConfig(data, lenth);
			break;
		case CMD_WR_SYS_CFG:	//设置系统配置 --0xF6
			ReturnVal = Debug_Set_SystemConfig(data, lenth);
			if(ReturnVal == FALSE)
			{
				return;
			}
			u1_printf("\r\n 等待重启...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			
			delay_ms(200);
			Sys_Soft_Reset();
			break;
//		
//		//单机配置输入通道================================================================
		case CMD_GET_OUT_STATE:		//查询输出控制状态--0x02
			Debug_Get_Control_State(USART1, data, lenth);
			break;
	
		case CMD_MANUAL_CTRL:		//手动控制 --0x03
			
			Init_Relay_Power();	
			Init_Relay_Port();
			delay_ms(80);
			Debug_Manual_Ctrl(USART1, data, lenth);
			delay_ms(80);
			Deinit_Relay_Power();	
			Deinit_Relay_Port();
		break;
		
		case CMD_INPUT_COUNT:	   //获取设备额定单元输入参量个数 --0x10
			Debug_Input_Count(data, lenth);
		break;
		case CMD_GET_INPUT:		  //读取一个单元输入参量配置信息 --0x11
			Debug_Get_Input(data, lenth);
		break;
		case CMD_SET_INPUT:		  //设置一个单元输入参量配置信息 --0x12
			Debug_Set_Input(data, lenth);
		break;
		case CMD_DEL_INPUT:	   //删除一个输入配置 --0xF7
			Debug_Delete_Input(data, lenth);
		break;
		//单机配置输出通道=================================================================
		case CMD_OUTPUT_COUNT:	 //获取设备额定单元输出通道个数	--0x13
			Debug_Output_Count(data, lenth);
			break;
		case CMD_GET_OUTPUT:	 //读取一个单元输出通道关联管脚配置信息--0x14
			Get_OutputChannel(data, lenth);
			break;
		case CMD_SET_OUTPUT:	//设置一个单元输出通道关联管脚配置信息--0x15
			Set_Output_Channel(data, lenth);
			break;
		case CMD_DEL_OUTPUT:   //删除一个输出配置  --0xF8
			Delete_Output_Channel(data, lenth);
			break;		
		
		case CMD_TEST_MODE:		//进入低功耗设备配置模式	关闭通信,不进入停止模式
			Debug_Enter_Test_Mode(data, lenth);		
			break;
		
		case CMD_RESET:			//设备重启
			u1_printf("\r\n 设备重启指令...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
	}
}


