#include "SensorManage.h"
#include "main.h"

#define		ERROR_485VALUE		0XEEEEEEEE

MAP_IN_ITEM  *map_in_list[HWL_INPUT_COUNT];	

static unsigned char s_RJYSensorFlag = FALSE;
static unsigned char s_RJYSensorAddr = 0;

static unsigned char s_ChlorophyllSensorFlag = FALSE;
static unsigned char s_ChlorophyllSensorAddr = 0;

unsigned char GetRJYSensorFlag(unsigned char *Addr)
{
	*Addr = s_RJYSensorAddr;
	return s_RJYSensorFlag;
}

unsigned char GetChlorophyllSensorFlag(unsigned char *Addr)
{
	*Addr = s_ChlorophyllSensorAddr;
	return s_ChlorophyllSensorFlag;
}

unsigned char ShowSensorName(unsigned char SensorID, unsigned char isTrue, float SensorVal)
{
	u32 error_mark = ERROR_485VALUE;
	
	switch(SensorID)
	{
		case KQW_ID: 		//Air Temperature
			u1_printf("\r\n Air Temperature");

		break;
		
		case KQS_ID:	 	//Air Humidity
			u1_printf("\r\n Air Humidity");

		break;

		case TRW_ID:		//Soil Temperature
			u1_printf("\r\n Soil Temperature");

			break;
		case TRS_ID:		//Soil Humidity
			u1_printf("\r\n Soil Humidity");

			break;
		case SOIL_EC_ID:		//Soil Electric
			u1_printf("\r\n Soil Electric");

			break;
		case SOIL_YF_ID:		//Soil Salinity
			u1_printf("\r\n Soil Salinity");

			break;
		case GZD_ID:		//Illuminance
			u1_printf("\r\n Illuminance");

		break;
		case CO2_ID:		//Carbon Dioxide
			u1_printf("\r\n Carbon Dioxide");

		break;
		case FS_ID:			  //Wind Speed
			u1_printf("\r\n Wind Speed");

			break;
		case FX_ID:			  //Wind Direction
			u1_printf("\r\n Wind Direction");

			break;
		case AIR_PRESS_ID:	  //Atmospheric Pressure
			u1_printf("\r\n Atmospheric Pressure");

			break;
		case PH_ID:			  //PH Value
			u1_printf("\r\n PH Value");

			break;
		case SYL_ID:			  //Water Pressure
			u1_printf("\r\n Water Pressure");

			break;
		case YW_ID:			  //Liquid Level
			u1_printf("\r\n Liquid Level");

			break;
		case YMSD_ID:			  //Leaf Wetness
			u1_printf("\r\n Leaf Wetness");

			break;
		case NOISE_ID:			  //Noise Decibel
			u1_printf("\r\n Noise Decibel");

			break;
		case H2S_ID:			  //H2S
			u1_printf("\r\n H2S");

			break;
		case NH3_ID:			  //NH3
			u1_printf("\r\n NH3");

			break;
		case PHOTOSYNTHETIC_ACTIVE_RADIATION_ID:			  //Photosynthetically Active Radiation
			u1_printf("\r\n Photosynthetically Active Radiation");

			break;
		case EVAPORATION_ID:			  //Evaporation
			u1_printf("\r\n Evaporation");

			break;
		case SOLAR_RADIATION_ID:			  //Solar Radiation
			u1_printf("\r\n Solar Radiation");

			break;
		case AIR_ION:			  //Air Ion
			u1_printf("\r\n Air Ion");

			break;	
		case O2_ID:			  //O2
			u1_printf("\r\n O2");

			break;
		case CH4_ID:			  //CH4
			u1_printf("\r\n CH4");

			break;
		case CO_ID:			  //CO
			u1_printf("\r\n CO");

			break;
		case NO2_ID:			  //NO2
			u1_printf("\r\n NO2");

			break;
		case SO2_ID:			  //SO2
			u1_printf("\r\n SO2");

			break;
		case SMOKE_ID:			  //Smoke
			u1_printf("\r\n Smoke");

			break;
		case HCL_ID:			  //HCL
			u1_printf("\r\n HCL");

			break;
		case RJY_ID:			  //Dissolved Oxygen
			u1_printf("\r\n Dissolved Oxygen");
			break;
		case COD_ID:			  //COD
			u1_printf("\r\n COD");

			break;
		case NH3_NH4_ID:			  //NH3 NH4
			u1_printf("\r\n NH3 NH4");

			break;
		case DDL_ID:			  //Water Electric
			u1_printf("\r\n Water Electric");

			break;
		case H2O_NH3_ID:			  //Water NH3 NH4
			u1_printf("\r\n Water NH3 NH4");

			break;
		case H2O_TURBIDITY_ID:			  //Turbidimeter
			u1_printf("\r\n Turbidimeter");

			break;
		case H2O_CL2_ID:			  //Water CL2
			u1_printf("\r\n Water CL2");

			break;
		case SOIL_M_ID:			  //Soil Tension
			u1_printf("\r\n Soil Tension");

			break;
		case JYL_ID:			  //RainFall
			u1_printf("\r\n RainFall");
		
			break;
	
		case LL_ID:			  //Flow
			u1_printf("\r\n Flow");

			break;
		
		case ORP_POTENTIOMETER_ID:			  
			u1_printf("\r\n ORP");

			break;	
		case CHLOROPHYLL_ID:			 
			u1_printf("\r\n Chlorophyll");

			break;	
		case LONGITUDE_ID:			  
			u1_printf("\r\n Longitude");

			break;	
		case LATITUDE_ID:			
			u1_printf("\r\n Latitude");

			break;	
		
		default:
			u1_printf(" No id\r\n");
		break;
	}
	
	if(isTrue)
	{
		if(SensorVal == ERROR_485VALUE)
		{	
			u1_printf(" Err\r\n");	
			return FALSE;	
		}
		else if(memcmp((uint8 *)&SensorVal, (uint8 *)&error_mark, sizeof(uint32)) == 0)
		{
			u1_printf(" Err\r\n");	
			return FALSE;
		}
		else
		{
			u1_printf(":%2.3f\r\n", SensorVal);
			return TRUE;
		}
	}
	return TRUE;
}

static void SetWaitingTime(unsigned char Time)
{
	if(g_ReportDataWaitingTime < Time)
	{
		g_ReportDataWaitingTime = Time;
	}
}

unsigned char hwl_input_init(unsigned int input_ch, unsigned char Display)
{
	MAP_IN_ITEM *item;
	
	item = get_map_in_item(input_ch);
	map_in_list[input_ch] = item;
	
	if (item == NULL) 
	{
		return 0;	
	}
	else
	{
		if(Display)
		{
			ShowSensorName(item->data_id, FALSE, 0);
			switch(item->data_id)
			{
				case KQW_ID: 		//Air Temperature
					SetWaitingTime(3);
					break;			
				case KQS_ID:	 	//Air Air Humidity
					SetWaitingTime(3);
					break;
				case TRW_ID:		//Soil Temperature
					SetWaitingTime(9);
					break;
				case TRS_ID:		//Soil Humidity
					SetWaitingTime(3);
					break;
				case SOIL_EC_ID:		//Soil Electric
					SetWaitingTime(19);
					break;
				case SOIL_YF_ID:		//Soil Salinity
					SetWaitingTime(13);
					break;
				case GZD_ID:		//Illuminance
					SetWaitingTime(3);
					break;
				case CO2_ID:		//Carbon Dioxide
					SetWaitingTime(30);
					break;
				case FS_ID:			  //Wind Speed
					SetWaitingTime(3);
					break;
				case FX_ID:			  //Wind Direction
					SetWaitingTime(3);
					break;
				case AIR_PRESS_ID:	  //Atmospheric Pressure
					SetWaitingTime(3);
					break;
				case PH_ID:			  //PH Value
					SetWaitingTime(15);
					break;
				case SYL_ID:			  //Water Pressure
					SetWaitingTime(5);
					break;
				case YW_ID:			  //Liquid Level
					switch(item->in_param[1])	//每个脉冲对应的Flow数值是多少。
					{
						case 0:
							u1_printf(" 2m:");
							break;
						case 1:
							u1_printf(" 5m:");
							break;
						case 2:
							u1_printf(" 10m:");
							break;
						case 3:
							u1_printf(" 20m:");
							break;
						case 4:
							u1_printf(" 50m:");
							break;
					}
					SetWaitingTime(3);
					break;
				case YMSD_ID:			  //Leaf Wetness
					SetWaitingTime(2);
					break;
				case NOISE_ID:			  //Noise Decibel
					SetWaitingTime(2);
					break;
				case H2S_ID:			  //H2S
					SetWaitingTime(2);
					break;
				case NH3_ID:			  //NH3
					SetWaitingTime(2);
					break;
				case PHOTOSYNTHETIC_ACTIVE_RADIATION_ID:			  //Photosynthetically Active Radiation
					SetWaitingTime(20);
					break;
				case EVAPORATION_ID:			  //Evaporation
					SetWaitingTime(2);
					break;
				case SOLAR_RADIATION_ID:			  //Solar Radiation
					SetWaitingTime(2);
					break;
					
				case AIR_ION:			  //Air Ion
					SetWaitingTime(70);

				break;	
				case O2_ID:			  //O2
					SetWaitingTime(10);
					break;
				case CH4_ID:			  //CH4
					SetWaitingTime(2);
					break;
				case CO_ID:			  //CO
					SetWaitingTime(2);
					break;
				case NO2_ID:			  //NO2
					SetWaitingTime(2);
					break;
				case SO2_ID:			  //SO2
					SetWaitingTime(2);
					break;
				case SMOKE_ID:			  //Smoke
					SetWaitingTime(30);
					break;
				case HCL_ID:			  //HCL
					SetWaitingTime(2);
					break;
				case RJY_ID:			  //Dissolved Oxygen
					s_RJYSensorAddr = item->in_param[2];
					s_RJYSensorFlag = TRUE;
					SetWaitingTime(5);
					break;
				case COD_ID:			  //COD
					SetWaitingTime(2);
					break;
				case NH3_NH4_ID:			  //NH3 NH4
					SetWaitingTime(2);
					break;
				case DDL_ID:			  //Water Electric
					SetWaitingTime(17);
					break;
				case H2O_NH3_ID:			  //水质NH3 NH4
					SetWaitingTime(15);
					break;
				case H2O_TURBIDITY_ID:			  //Turbidimeter
					SetWaitingTime(15);
					break;
				case H2O_CL2_ID:			  //Water CL2
					SetWaitingTime(15);
					break;
				case SOIL_M_ID:			  //Soil Tension
					SetWaitingTime(5);
					break;
				
				case ORP_POTENTIOMETER_ID:		//ORP
					SetWaitingTime(30);
					break;
				
				case CHLOROPHYLL_ID:			  //Chlorophyll
					SetWaitingTime(30);
					s_ChlorophyllSensorAddr = item->in_param[2];
					s_ChlorophyllSensorFlag = TRUE;
					
					break;
			}
			
			if(item->in_param[5])
			{
				u1_printf(" DY:");
			}
			u1_printf(" Num=%d,Type=0x%02X,Channel=%02d,RS485 Addr=0x%02X,Register Addr=0x%04X\r\n",input_ch+1 , item->data_id, item->in_param[0]+1, item->in_param[2], (item->in_param[3]<<8) + item->in_param[4]);
			
		}
	
	}
		return 1;
}


//根据输入配置信息从具体绑定的硬件上获取数据(如果能正确映射,否则通过函数返回结果说明获取失败)
unsigned int hwl_get_input_value(unsigned int input_ch, DATA_INFO *data_info)
{
	MAP_IN_ITEM *item;
	
	float result = ERROR_VALUE;
	uint32 error_mark = 0xEEEEEEEE;
	
	if (input_ch >= HWL_INPUT_COUNT) return 0;
	
	item = get_map_in_item(input_ch);

	if (item == NULL) return 0;

	result = GetSensorData(input_ch);
	
//	if(ShowSensorName(item->data_id, TRUE, result))
//	{		
//		result = result + item->compensation;	//修正值
//	}
				
	if(memcmp((uint8 *)&result, (uint8 *)&error_mark, sizeof(uint32)) != 0)
	{
//		if(range_spec->high_limit || range_spec->low_limit)	//若设定了上下限,则最终数据不会超出其范围,若未设置,即为读数。
//		{
//			if(result > range_spec->high_limit)
//			{
//				result = range_spec->high_limit;
//				u1_printf(" Data upper limit%.2f\r\n", range_spec->high_limit);
//			}
//			else if(result < range_spec->low_limit)
//			{
//				result = range_spec->low_limit;
//				u1_printf(" Data lower limit%.2f\r\n", range_spec->low_limit);
//			}
//		}
	}
	
	data_info->value = result;	
	data_info->data_id = item->data_id;
	data_info->attr_ptr = (void *)map_in_list[input_ch];
	
	return 1;
}

















