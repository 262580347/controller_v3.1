#include "main.h"
#include "Map_out.h"

#define		ADDR_OFFSET	20

#define		MAP_OUT_ITEM_SIZE		64

#define 	MAP_OUT_BASE_ADDR	(EEPROM_BASE_ADDR + ADDR_OFFSET*EEPROM_BLOCK_SIZE)  	//WLAN密码存储区

#define		CHANNEL_CONFIG_MAX_SIZE 	6

typedef __packed struct
{
	unsigned char magic;
	unsigned short length;
	unsigned char chksum;
}MAP_OUT_TAG;

static unsigned int Check_Area_OK(unsigned int base_addr)
{
	MAP_OUT_TAG tag;
	unsigned char chksum;
	unsigned char data[4];
	
	EEPROM_ReadBytes(base_addr, data, 4);

	tag.magic = data[0];
	tag.length = (data[1]) + ((data[2]) << 8);
	tag.chksum = data[3] ;
	
	if (tag.magic != 0x55)
	{
//		u1_printf("!0x55:0x%X\r\n", base_addr);
		return 0;
	}
	
	if (tag.length > (MAP_IN_ITEM_SIZE - sizeof(MAP_OUT_TAG))) return 0;
	
	
//	chksum = Calc_Checksum((unsigned char *)(base_addr+sizeof(MAP_IN_TAG)), tag.length);
	chksum = EEPROM_CheckSum((base_addr+sizeof(MAP_OUT_TAG)), tag.length);
	if (tag.chksum != chksum) 		
	{
//		u1_printf("chksum:%02X != tag.chksum:%02X\r\n", chksum, tag.chksum);
		return 0;
	}
	
	return 1;
}

//=============================================================================
//设置输出通道参数--一个通道的参数为64字节,12个通道是768个字节,不超过一个扇区。
//通道的配置数据都在一个扇区内操作操作
//=============================================================================
static 	unsigned char map_buf[MAP_OUT_ITEM_SIZE*CHANNEL_CONFIG_MAX_SIZE];
unsigned int SetChannelConfig(unsigned int index, MAP_OUT_ITEM *item)
{
	int mod;
	unsigned int addr;
	u8 i;
	u8 data[2];
	static MAP_OUT_TAG tag;		//存储头格式
	static MAP_OUT_TAG *p_tag;

	p_tag = &tag;
	
	mod = index;
	//读一个扇区数据出来
	for(i=0;i<OUTPUT_CHANNEL_COUNT;i++)
	{
		addr = MAP_OUT_BASE_ADDR + i*MAP_OUT_ITEM_SIZE;
		if (Check_Area_OK(addr))	//检查格式是否有效
		{			
			EEPROM_ReadBytes(addr+1, data, 2);
			
			tag.length = data[0] + (data[1] << 8);
			
//			u1_printf("tag.length1:%d \r\n", tag.length);
			
			EEPROM_ReadBytes(addr, &map_buf[i*MAP_OUT_ITEM_SIZE], tag.length+sizeof(MAP_OUT_TAG));	
		}		
	}

	if (item)
	{
		//添加或修改
//		u1_printf(" Add\r\n");
		p_tag = (MAP_OUT_TAG *)&map_buf[mod*MAP_OUT_ITEM_SIZE];
		p_tag->magic = 0x55;
		p_tag->length = sizeof(MAP_OUT_ITEM);
		memcpy(&map_buf[mod*MAP_OUT_ITEM_SIZE + sizeof(MAP_OUT_TAG)], item, p_tag->length);
		p_tag->chksum = Calc_Checksum(&map_buf[mod*MAP_OUT_ITEM_SIZE + sizeof(MAP_OUT_TAG)], p_tag->length);
	}
	else
	{
		//删除配置
//		u1_printf(" Delete\r\n");
		EEPROM_EraseWords(index+ADDR_OFFSET);	
		
		return 1;
	}
	
//	u1_printf(" Add2\r\n");	
	addr = MAP_OUT_BASE_ADDR + mod*MAP_OUT_ITEM_SIZE;
	p_tag = (MAP_OUT_TAG *)&map_buf[mod*MAP_OUT_ITEM_SIZE];
	EEPROM_WriteBytes(addr, (unsigned char *)&map_buf[mod*MAP_OUT_ITEM_SIZE], p_tag->length+sizeof(MAP_OUT_TAG));
	
	return 1;
}

static u8 s_Map_In_Buff[MAP_OUT_ITEM_SIZE];

MAP_OUT_ITEM *GetChannelConfigInfo( uint16 index,unsigned char * ChannelConfig )
{
	unsigned int addr;
	MAP_OUT_TAG *tag;
	MAP_OUT_ITEM	*item;

	addr = MAP_OUT_BASE_ADDR + MAP_OUT_ITEM_SIZE*index;
	
	if ( Check_Area_OK(addr) == 0 ) 
	{
//		u1_printf("%d No OK\r\n", index);
		return 0;
	}

	EEPROM_ReadBytes(addr, s_Map_In_Buff, sizeof(MAP_OUT_TAG));
	
	tag = (MAP_OUT_TAG *)&s_Map_In_Buff;
	
	if(tag->length != 0)
	{

		EEPROM_ReadBytes(addr + sizeof(MAP_OUT_TAG), &s_Map_In_Buff[sizeof(MAP_OUT_TAG)], sizeof(MAP_OUT_ITEM));		
			
		item = (MAP_OUT_ITEM *)&s_Map_In_Buff[sizeof(MAP_OUT_TAG)];
	}
	
	memcpy(ChannelConfig, &s_Map_In_Buff[sizeof(MAP_OUT_TAG)], sizeof(MAP_OUT_ITEM));

	return item;
}


