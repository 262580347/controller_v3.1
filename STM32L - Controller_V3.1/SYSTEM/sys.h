#ifndef __SYS_H
#define __SYS_H	 

#include "stm32l1xx.h"
#include "general_type.h"
#include "stdarg.h"	 
#include "stdio.h"
#include "mytype.h"


void Sys_Soft_Reset(void);      //系统软复位

void MY_NVIC_SetVectorTable(u32 NVIC_VectTab, u32 Offset);//设置偏移地址
void MY_NVIC_PriorityGroupConfig(u32 NVIC_Group);//设置NVIC分组
void MY_NVIC_Init(u8 NVIC_PreemptionPriority,u8 NVIC_SubPriority,u8 NVIC_Channel,u8 NVIC_Group);//设置中断

//以下为汇编函数
void WFI_SET(void);		//执行WFI指令
void INTX_DISABLE(void);//关闭所有中断
void INTX_ENABLE(void);	//开启所有中断
void MSR_MSP(u32 addr);	//设置堆栈地址

unsigned short PackMsg(u8* srcbuf,u16 srclen,u8* destbuf,u16 destsize);
void Int32ToArray( unsigned char *pDst, unsigned int value );
unsigned char CheckHdrSum(CLOUD_HDR *pMsg, u8 *data, u8 lenth);
float encode_float(float value);
float Mid_Filter(float * filter_data, int16 len);

float GetAverageValue(float * data, unsigned char len);
#endif











