#ifndef		_EXIT_H_
#define		_EXIT_H_

#define		EXIT_IRQN			EXTI15_10_IRQn

#define		EXIT_LINE_PIN		EXTI_Line15

#define		EXIT_PIN_SOURCE		EXTI_PinSource15

void EXTI_Aux_Config(void);

void EXTI_Aux_Reset(void);

void EXTI17_Reset(void);





#endif


