/**********************************
说明：调试使用的串口代码
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

unsigned char s_ComTestFlag = FALSE;

unsigned char GetComTestFlag(void)
{
	return s_ComTestFlag;
}

void SetComTestFlag(unsigned char isTrue)
{
	s_ComTestFlag = isTrue;
}

void Create_Alive_Message(unsigned int Device_ID)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(308);							//通信协议版本号
	hdr->device_id = swap_dword(Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	send_buf[len++] = 	SENSOR_TYPE_GATEWAY;	//传感器网关  200
	
	send_buf[len++] = 1;	//通道1
	
	send_buf[len++] = 0;
	send_buf[len++] = 100;	//协议版本
	
	send_buf[len++] = XML_BAT_VOLTAGE;	//电压标识
	send_buf[len++] = 84;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
		
	nMsgLen = PackMsg(send_buf, len, AlivePack, 200);	//数据包转义处理	
	u1_printf("[模拟心跳] -> %d(%d): ", Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	
}


static void COM1Protocol(void)
{
	unsigned char PackBuff[100], DataPack[100];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}

void TaskForDebugCOM(void)
{
	static u8  Controlcmd[7] = {0x01, 0x00, 0x01, 0x05, 0x01, 0x05, 0x00},  isOn = FALSE;
	u8 ch_ID, DelayTime = 0;
	unsigned int l_DeviceID = 0;
	u8 i, j;
	unsigned int  databuf4[64];

	
	if(g_UartRxFlag == TRUE)
	{
		g_UartRxFlag = FALSE;
		

		if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n RESET CMD\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TEST MODE", g_USART_RX_CNT) == 0)
		{
			if(GetComTestFlag() == FALSE)
			{
				u1_printf("\r\n 进入调试模式,通信关闭,设备会持续耗电\r\n");
				SetComTestFlag(TRUE);
			}
			else
			{
				u1_printf("\r\n 退出调试模式,通信恢复\r\n");
				SetComTestFlag(FALSE);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHTO ", 5) == 0)
		{					
			ch_ID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			if(isOn == FALSE)
			{
				isOn = TRUE;
				Controlcmd[4] = 0x01;
				Controlcmd[6] = 0x00;
			}
			else
			{
				isOn = FALSE;
				Controlcmd[4] = 0x00;
				Controlcmd[6] = 0x01;	
			}
			if(ch_ID > 0 && ch_ID < 5)
			{
				Init_Relay_Power();		//开机时给继电器电容充电,会产生瞬时电压跌落
				Init_Relay_Port();
				delay_ms(50);	
				u1_printf(" 串口控制 -> 通道(%d)翻转\r\n", ch_ID);
				output_manual_action(ch_ID-1, Controlcmd);
				delay_ms(10);
				Deinit_Relay_Power();
				Deinit_Relay_Port();
			}
			else
			{
				u1_printf(" 错误的通道值\r\n");
			}					
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHON ", 5) == 0)
		{					
			ch_ID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			Controlcmd[4] = 0x01;
			Controlcmd[6] = 0x00;
			if(ch_ID > 0 && ch_ID < 5)
			{
				delay_ms(50);	
				u1_printf(" 串口控制 -> 通道(%d)开启\r\n", ch_ID);
				output_manual_action(ch_ID-1, Controlcmd);
				delay_ms(10);
			}
			else
			{
				u1_printf(" 错误的通道值\r\n");
			}					
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHST ", 5) == 0)
		{
			
			ch_ID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			Controlcmd[4] = 0x00;
			Controlcmd[6] = 0x00;
			if(ch_ID > 0 && ch_ID < 5)
			{
				delay_ms(50);	
				u1_printf(" 串口控制 -> 通道(%d)停止\r\n", ch_ID);
				output_manual_action(ch_ID-1, Controlcmd);
				delay_ms(10);
			}
			else
			{
				u1_printf(" 错误的通道值\r\n");
			}			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHOF ", 5) == 0)
		{
			
			ch_ID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			Controlcmd[4] = 0x02;
			Controlcmd[6] = 0x00;
			if(ch_ID > 0 && ch_ID < 5)
			{
				delay_ms(50);	
				u1_printf(" 串口控制 -> 通道(%d)关闭\r\n", ch_ID);
				output_manual_action(ch_ID-1, Controlcmd);
				delay_ms(10);
			}
			else
			{
				u1_printf(" 错误的通道值\r\n");
			}			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Alive ", 6) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			Create_Alive_Message(l_DeviceID);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "ShowTime", g_USART_RX_CNT) == 0)
		{
			RTC_TimeShow();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Init_Relay_Power", g_USART_RX_CNT) == 0)
		{
			u1_printf("Init_Relay_Power\r\n");
			Init_Relay_Power();	
			CTR_VCC_ON();
			Init_Relay_Port();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Deinit_Relay_Power", g_USART_RX_CNT) == 0)
		{
			u1_printf("Deinit_Relay_Power\r\n");
			Deinit_Relay_Power();	
			Deinit_Relay_Port();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "PowerDetect", g_USART_RX_CNT) == 0)
		{
			u1_printf("PowerDetect\r\n");
			g_PowerDetectFlag = FALSE;
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "LORA WORK", g_USART_RX_CNT) == 0)
		{
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA SLEEP", g_USART_RX_CNT) == 0)
		{
			LORA_SLEEP_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA ROUSE", g_USART_RX_CNT) == 0)
		{
			LORA_ROUSE_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA LOWPOWER", g_USART_RX_CNT) == 0)
		{
			LORA_LOWPOWER_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "STM32 STOP", g_USART_RX_CNT) == 0)
		{
			EnterStopMode(10);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SIM800C", g_USART_RX_CNT) == 0)
		{
			u1_printf("SIM800C ON/OFF \r\n");
			GPIO_ToggleBits(SIM800C_ENABLE_TYPE,SIM800C_ENABLE_PIN);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "SIM_Send_Alive", g_USART_RX_CNT) == 0)
		{
			u1_printf("SIM_Send_Alive\r\n");
			Mod_Send_Alive(SIM_COM);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "SIM_Report_Data", g_USART_RX_CNT) == 0)
		{
			u1_printf("SIM_Report_Data\r\n");
			Mod_Report_Data(SIM_COM);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Start Charging", g_USART_RX_CNT) == 0)
		{
			u1_printf(" Start Charging\r\n");
			SetBQ24650ENFlag(TRUE);	//关闭充电芯片的唯一入口	
			g_PowerDetectFlag = FALSE;
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "STATE ", 6) == 0)
		{
			LORA_WORK_MODE();
			DelayTime = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			delay_ms(DelayTime)	;		
			Debug_Report_Status(LORA_COM);		
			u1_printf(" 延时测试 -> %dms\r\n", DelayTime);
			LORA_SLEEP_DELAY();
			LORA_LOWPOWER_MODE();			
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "EraseAll", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase all EEPROM data\r\n");
			for(i=0; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
		}
		else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
		{
			ShowLogContent();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase EEPROM Data\r\n");
			EEPROM_EraseWords(9);
			for(i=30; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
			LogStorePointerInit();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "READ4", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read4\r\n");
					
			for(j=0; j<64; j++)
			{
				EEPROM_ReadWords(EEPROM_BASE_ADDR + j*64, databuf4, 16); 
				
				for(i=0; i<16; i++)
				{
					u1_printf("%08X ", databuf4[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "GPRS_Alive", g_USART_RX_CNT) == 0)
		{
			Mod_Send_Alive(GPRS_COM);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPRS_Data", g_USART_RX_CNT) == 0)
		{
			Mod_Report_Data(GPRS_COM);
		}	
		else
		{		
			if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_BUF[g_USART_RX_CNT-1] == EOF_VAL && g_USART_RX_CNT >= 15)
			{
				COM1Protocol();
			}
		}
		Clear_Uart1Buff();
	}	
	

}










