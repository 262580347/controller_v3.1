#include "main.h"
#include "GPRS.h"

#define COM_DATA_SIZE	256


void GPRS_Process(void)
{
	static unsigned short s_LastTime, s_LastDataTime = 0, s_PowerTime = 0;
	static unsigned char s_First = FALSE, s_State = 1, s_RetryCount = 0;
	static u8 s_OverCount;
	SYSTEMCONFIG *p_sys;
	GPIO_InitTypeDef GPIO_InitStructure;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		GPIO_InitStructure.GPIO_Pin = GPRS_PWR_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(GPRS_PWR_TYPE, &GPIO_InitStructure);

		GPIO_ResetBits(GPRS_PWR_TYPE,GPRS_PWR_PIN);
		
		u1_printf("\r\n Eport Power off delay 1s...\r\n");
		delay_ms(1000);
		GPIO_SetBits(GPRS_PWR_TYPE,GPRS_PWR_PIN);
		
		USART3_Config(115200);
		s_LastTime = GetSystem10msCount();
	}
	
	switch(s_State)
	{
		case 1: //等待连上网络
			
			if(GetTCModuleReadyFlag())
			{
				s_LastTime = GetSystem10msCount();
				s_State = 5;
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1200)
			{
				Mod_Send_Alive(GPRS_COM);
				delay_ms(100);
				Mod_Report_Data(GPRS_COM);
				delay_ms(100);
				Debug_Report_Status(GPRS_COM);
				s_LastTime = GetSystem10msCount();
				s_RetryCount++;
				if(s_RetryCount >= 60)
				{
					u1_printf("\r\n 超时未连接上网络   等待重启...\r\n");
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();
				}
			}			
		break;
		
		case 2:
			if(GetTCModuleReadyFlag())
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				s_State--;
			}
		break;
		
		case 3:
			g_TCRecStatus.AliveAckFlag = FALSE;
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
			u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
			Debug_Report_Status(GPRS_COM);
			delay_ms(50);
			Mod_Send_Alive(GPRS_COM);
			s_State++;
			s_LastTime = GetSystem10msCount();
		break;

		case 4:
			if(g_TCRecStatus.AliveAckFlag)
			{
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
				{
					s_State--;
					s_LastTime = GetSystem10msCount();
					u1_printf("\r\n 超时未收到心跳回复包\r\n");
					s_OverCount++;
					if(s_OverCount >= 5)
					{
						s_OverCount = 0;
						s_State = 1;
						SetTCModuleReadyFlag(FALSE);
						SetOnline(FALSE);
						u1_printf("\r\n 切换至离线模式\r\n");
					}
				}
			}
		break;	
					
		case 5:
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
			{
				s_State = 2;
			}	
			
			if( CalculateTime(GetSystem10msCount(),s_LastDataTime) >= p_sys->Data_interval*100 )
			{
				s_LastDataTime = GetSystem10msCount();
				Mod_Report_Data(GPRS_COM);
			}
			
			if( CalculateTime(GetSystem10msCount(),s_PowerTime) >= 6000 )
			{
				s_PowerTime = GetSystem10msCount();
				StartGetBatVol();			//电池电压检测
			}			
		break;		
	}
	
}

