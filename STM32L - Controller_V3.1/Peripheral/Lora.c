/**********************************
说明：LORA通信模块,使用500ms唤醒间隔,TXD,AUX推挽输出,RXD 上拉输入
	  FEC开启,20dBm发射功率,2.4K空中速率
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "Lora.h"
#include "main.h"

#define COM_DATA_SIZE	256

#define	MAX_DATA_BUF	256

#define		LORA_BUFF_MAX		250

#define		PACK_INIT_NUM	255
static unsigned char s_RecDataBuff[MAX_DATA_BUF]; 

//#define	LORA_SPEED_INIT 	0x3A	//2.4K
#define	LORA_SPEED_INIT 	0x3D		//19.2K
#define	LORA_OPTION			0xC0

LORA_CONFIG g_LoraConfig;
LORA_CONFIG *p_Lora_Config;

static u8 s_RecDataPackFlag = FALSE;

static u8 s_RegistID = 0;

static u8 s_Data_Interval = 2;

unsigned char g_RecAck = FALSE;

static u8 s_LoraState = FALSE;

static u8 s_LEDFlag = FALSE;

static u8 s_TCProtocolForLoraRunFlag = FALSE;

static u8 s_RecControlCmdFlag = FALSE;

unsigned char GetRecControlCmdFlag(void)
{
	return s_RecControlCmdFlag;
}

void SetRecControlCmdFlag(unsigned char isTrue)
{
	s_RecControlCmdFlag = isTrue;
}

void SetTCProtocolForLoraRunFlag(unsigned char isTrue)
{
	s_TCProtocolForLoraRunFlag = isTrue;
	Clear_Uart2Buff();
}

unsigned char GetLEDFlag(void)
{
	return s_LEDFlag;
}

void SetLEDFlag(unsigned char isTrue)
{
	s_LEDFlag = isTrue;
}

unsigned char GetLoraState(void)
{
	return s_LoraState;
}

unsigned char GetRecDataPackFlag(void)
{
	return s_RecDataPackFlag;
}

const LORA_CONFIG DefaultLoraConfig = 
{
	0x0f,
	0xff,
	LORA_SPEED_INIT,
	0x13,
	LORA_OPTION,
};

void Lora_Send_Data(unsigned short int Tagetaddr, unsigned char channel, unsigned char *data, unsigned int len)
{
	u8 LoraPack[256];
	u8 Lora_lenth;
	
	LoraPack[0] = Tagetaddr>>8;
	LoraPack[1] = Tagetaddr;
	LoraPack[2] = channel;
	Lora_lenth = 3;
	memcpy(&LoraPack[Lora_lenth], data, len);
	
	Lora_lenth += len;
	
	USART2_DMA_Send(LoraPack, Lora_lenth);
}

void LoraPort_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = LORA_AUX_PIN;	//LORA  AUX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_AUX_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
	

}

static u8 LoraInit()
{
	static u8 s_State = 0, Reset_LoraConfig_Flag = FALSE;
	static unsigned short s_LastTime = 0, nMain10ms;
	u8 i, sendbuf[6];
	SYSTEMCONFIG *p_sys;
	const u8 CMD_C1[3] = {0xc1, 0xc1, 0xc1};
	const u8 CMD_C3[3] = {0xc3, 0xc3, 0xc3};
	const u8 CMD_C5[3] = {0xc5, 0xc5, 0xc5};
	const u8 CMD_F3[3] = {0xf3, 0xf3, 0xf3};
	
	p_sys = GetSystemConfig();	
	nMain10ms = GetSystem10msCount();
	
	switch(s_State)
	{
		case 0:			
			if(CalculateTime(nMain10ms, s_LastTime) >= 3) 
			{
				u1_printf("\r\n [Lora]Sleep Mode...\r\n");
				LORA_SLEEP_MODE();
				s_LastTime = nMain10ms;
				s_State++;	
			}	
		break;
		case 1:
			u1_printf("\r\n [Lora]Software Version:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_F3, 3);

			while(1)
			{
				nMain10ms = GetSystem10msCount();
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				
				if(CalculateTime(nMain10ms, s_LastTime) >= 20) 
				{
					s_LastTime = nMain10ms;					
					u1_printf("\r\n [Lora]获取版本超时\r\n");
					s_LoraState = FALSE;
					break;
				}
			
			}	
			s_State++;
			Clear_Uart2Buff();	
		break;
		case 2:
			u1_printf("\r\n [Lora]Hardware Version:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C3, 3);

			while(1)
			{
				nMain10ms = GetSystem10msCount();
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				if(CalculateTime(nMain10ms, s_LastTime) >= 20) 
				{
					s_LastTime = nMain10ms;					
					u1_printf("\r\n [Lora]获取版本超时\r\n");
					s_LoraState = FALSE;
					break;
				}

			}	
			s_State++;
			g_USART2_RX_CNT = 0;

		break;
		case 3:
			u1_printf("\r\n [Lora]电源电压：");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C5, 3);

			while(1)
			{
				nMain10ms = GetSystem10msCount();
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					u1_printf(" %1.3fV",(g_USART2_RX_BUF[1]*256 + g_USART2_RX_BUF[0])/1000.0);
					g_USART2_RX_CNT = 0;
					u1_printf("\r\n");		
					break;
				}
				if(CalculateTime(nMain10ms, s_LastTime) >= 20) 
				{
					s_LastTime = nMain10ms;					
					u1_printf("\r\n [Lora]获取电源电压超时\r\n");
					s_LoraState = FALSE;
					break;
				}
			}	
			s_State++;
			Clear_Uart2Buff();
			delay_ms(80);
		break;

		case 4:	
			u1_printf("\r\n [Lora]配置：");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C1, 3);


			while(1)
			{
				nMain10ms = GetSystem10msCount();
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;					
					for(i=1; i<g_USART2_RX_CNT; i++)
						u1_printf("%02X ", g_USART2_RX_BUF[i]);
					u1_printf("\r\n");
					if(((p_sys->Node)&0xffff) != ((g_USART2_RX_BUF[1]<<8) + g_USART2_RX_BUF[2]) || p_sys->Channel != g_USART2_RX_BUF[4] || g_USART2_RX_BUF[3] != LORA_SPEED_INIT || g_USART2_RX_BUF[5] != LORA_OPTION )					
					{
						Reset_LoraConfig_Flag = TRUE;
						u1_printf("\r\n 与设定值不符,重新配置LORA参数\r\n");
					}					
					else
					{
						s_State++;
						s_LoraState = TRUE;
						break;
					}
				}
						
					if(Reset_LoraConfig_Flag)			//重新配置LORA参数,系统会重启
					{
						Reset_LoraConfig_Flag = FALSE;
						sendbuf[0] = 0xC0;
						sendbuf[1] = (p_sys->Node >> 8)&0xff;
						sendbuf[2] = p_sys->Node &0xff;
						sendbuf[3] = LORA_SPEED_INIT;
						if(p_sys->Channel <= 31)
							sendbuf[4] = p_sys->Channel;
						else
							sendbuf[4] = 15;
						sendbuf[5] = LORA_OPTION;		
						Uart_Send_Data(LORA_COM, sendbuf, sizeof(sendbuf));
						s_LastTime = nMain10ms;	

						while(1)
						{
							nMain10ms = GetSystem10msCount();
							if(g_Uart2RxFlag == TRUE)
							{
								g_Uart2RxFlag = FALSE;
								if(g_USART2_RX_BUF[0] == 'O' && g_USART2_RX_BUF[1] == 'K' )
								{
									
									s_State++;
									u1_printf("\r\n [Lora]重新配置Lora参数成功\r\n");		

									LORA_WORK_DELAY();
									while (DMA_GetCurrDataCounter(DMA1_Channel4));
									Sys_Soft_Reset();
								}	
								else
								{
									u1_printf("\r\n [Lora]重新配置Lora失败\r\n");	
									s_State++;
								}
								g_USART2_RX_CNT = 0;								
								break;
							}
							
							if(CalculateTime(nMain10ms, s_LastTime) >= 30) 
							{
								s_LastTime = nMain10ms;					
								u1_printf("\r\n [Lora]重新配置LORA超时,Lora初始化失败...\r\n");
								return TRUE;
							}
						}
						break;
					}

				if(CalculateTime(nMain10ms, s_LastTime) >= 30) 
				{
					s_LastTime = nMain10ms;		
					SetLogErrCode(LOG_CODE_NOMODULE);
					StoreOperationalData();					
					u1_printf("\r\n [Lora]获取配置超时\r\n");
					s_LoraState = FALSE;
					Clear_Uart2Buff();
					s_State++;
					break;
				}
		
			}				
		break;
		case 5:
			if(CalculateTime(nMain10ms, s_LastTime) >= 20) 
			{
				s_LastTime = nMain10ms;		
				u1_printf("\r\n [Lora]进入工作模式...\r\n");
				LORA_WORK_MODE();
				return TRUE;	
			}
		
		default:
			
		break;
	}
	return FALSE;
}

//测试指令应答,用于检测设备是否在线
void Lora_TEST_ACK(unsigned char CMD)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AckPack[64];
	unsigned int len = 0;
	static uint16 seq_no = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	send_buf[len++] = 0X01;
	send_buf[len++] = 0XF4;
	
	send_buf[len++] = p_sys->Device_ID >> 24;
	send_buf[len++] = p_sys->Device_ID >> 16;	
	send_buf[len++] = p_sys->Device_ID >> 8;
	send_buf[len++] = p_sys->Device_ID;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 2;

	send_buf[len++] = CMD;									//命令字
	
	send_buf[len++] = 'O';
	send_buf[len++] = 'K';

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AckPack, 100);		//数据包转义处理	
	u1_printf("\r\n [Lora][测试指令应答OK] -> %d(%d): ", p_sys->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AckPack[i]);
	}
	u1_printf("\r\n");	

	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, AckPack, nMsgLen);
}

unsigned char JudgmentTimePoint(unsigned char Num, unsigned int *RemainingTime)
{
	unsigned char Minute, Second;
	unsigned short TimePoint = 0;
	static unsigned int s_RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	Minute = RTC_TimeStructure.RTC_Minutes%s_Data_Interval;
	Second = RTC_TimeStructure.RTC_Seconds;
	Num = Num%(s_Data_Interval*60/STOP_TIME);
	TimePoint = Num*STOP_TIME;	

	if(Minute*60 + Second >= TimePoint)
	{
		*RemainingTime = s_Data_Interval*60 - (Minute*60 + Second - TimePoint);		
	}
	else
	{
		*RemainingTime	 = TimePoint - (Minute*60 + Second);
	}
		
	if( (Minute*60 + Second >= TimePoint) && (Minute*60 + Second < TimePoint + STOP_TIME) && (DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) > 30) )
	{
		s_RTCTime = GetRTCSecond();			//到达唤醒时间点
		return TRUE;
	}
	else
	{	
		return FALSE;
	}
}

void LoraProcess(unsigned short nMain10ms)
{
	static u8 RunOnce = FALSE,  s_State = 1, s_RetryCount = 0, s_TimeToRun = FALSE;
	static u16 s_LastTime = 0, s_RebootCount = 0;
	static unsigned int RemainingTime = 0, s_RTCTime = 99999;
	unsigned char SleepTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
		
	if(RunOnce == FALSE)
	{	
		RunOnce = TRUE;
		
		SetTCProtocolForLoraRunFlag(FALSE);
		
		LoraPort_Init();	//初始化端口
		
		LORA_SLEEP_MODE();
				
		LORA_PWR_OFF();
		
		delay_ms(50);	
		
		LORA_PWR_ON();
		
		delay_ms(50);
		
		USART2_Config(9600);		//9600才可配置系统参数
				
		u1_printf("\r\n Init LORA\r\n");
		while(LoraInit() == FALSE)		//初始化LORA
		{
			
		}
			
		SetTCProtocolForLoraRunFlag(TRUE);
		
		USART2_Config(LORA_BAND_RATE);
		
		s_LastTime = nMain10ms;
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
		srand(RTC_TimeStructure.RTC_Seconds+nMain10ms); 	
		s_RegistID = rand() % 10;		//初始化时产生0~9的随机序号
		u1_printf("\r\n Random ID:%d\r\n", s_RegistID);
		
		LORA_WORK_MODE();
		
		return;
	}
	
	switch(s_State)
	{
		case 1:
			s_State++;
			LORA_WORK_MODE();
		break;
		
		case 2:						//等待电压检测完成
			if(g_PowerDetectFlag)
			{
				s_State++;
			}
		break;
		
		case 3:							//上报数据
				LORA_WORK_MODE();		
				LORA_WORK_DELAY();				
				Debug_Report_Status(LORA_COM);		//上报控制端口状态			
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();		
		break;
			
		case 4:
			if(s_RecDataPackFlag)	//收到应答,进入休眠
			{
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_RebootCount = 0;
				s_RetryCount = 0;
				if(g_ArriveReportingTime == FALSE)
				{
					SetReCMDFlag(FALSE);
				}
				SetOnline(TRUE);	
				SetLogErrCode(LOG_CODE_SUCCESS);
				StoreOperationalData();
			}
			else if((s_RetryCount <= 1) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= (160 + s_RegistID*2)))//第一次上报等待160ms + 0~500ms
			{
				s_RetryCount++;
				s_State--;		
			}
			else if((s_RetryCount == 2) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 160))//重发等待160ms
			{				
				s_RetryCount = 0;
				s_State++;
				SetReCMDFlag(FALSE);
				s_RebootCount++;
				u1_printf("\r\n 3次上报失败,休眠. Count:%d\r\n", s_RebootCount);		
				SetOnline(FALSE);	
				SetLogErrCode(LOG_CODE_GATEWAYNOACK);
				StoreOperationalData();				
			}
		break;
			
		case 5:
			if(s_RebootCount >= 10)
			{
				s_RebootCount = 0;
				u1_printf(" 10次上报数据失败,设备重启\r\n");
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				LORA_WORK_DELAY();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();				
			}
			
			s_LastTime = nMain10ms;
			if((g_PowerDetectFlag ) && (GetReCMDFlag() == FALSE))	//是否进入休眠
			{
				JudgmentTimePoint(s_RegistID, &RemainingTime);	//计算剩余唤醒时间
				u1_printf("%ds\r\n", RemainingTime);
				if(RemainingTime <= 10)					//精确计算出下一次的唤醒时长,在时区的最开始时间唤醒
				{
					SleepTime = RemainingTime + 3;
					
					if(SleepTime == 3)			//不休眠
					{
						s_RTCTime = GetRTCSecond();
						s_TimeToRun = TRUE;
						s_State++;	
						break;
					}
					else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) > 30)	//两次上报间隔需小于30s
					{
						s_RTCTime = GetRTCSecond();
						s_TimeToRun = TRUE;
					}	
				}
				else
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					SleepTime = STOP_TIME - RTC_TimeStructure.RTC_Seconds%10 + 3;
//					u1_printf("Sleep:%ds\r\n", SleepTime);
				}
				
				SetStopModeTime(TRUE, SleepTime);	//进入停止模式入口
			}		
			s_State++;				
		break;	
		
		case 6:
			if(s_TimeToRun)	//判断是否到达上报数据时间点
			{
				USART2_Config(LORA_BAND_RATE);		//直接配置LORA串口寄存器,在最短时间内捕捉唤醒消息
				SetLogErrCode(LOG_CODE_CLEAR);	
				s_State++;
				s_RetryCount = 0;
				s_LastTime = nMain10ms;
				SetControlTimeFlag(FALSE);
				s_TimeToRun = FALSE;
			}	
			else
			{
				s_LastTime = GetSystem10msCount();
				s_State--;
			}
		break;
					
		case 7:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1)
			{
				s_State = 1;
			}
		break;
	}
}

void OnRecLoraData(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg)	
{
	u8 sendbuf[100], sendlenth, i, ReturnVal = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	CLOUD_HDR *hdr;
	
	LED_NET_FLASH();	//收到网关应答,点亮NET
	
	Clear_Uart2Buff();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	
	if(pMsg->Length > LORA_BUFF_MAX)
	{
		u1_printf(" Buf Length Over(%d)!!!!!!!!!!!!\r\n", pMsg->Length);
		return;
	}
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][LORA]<-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
	RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->Protocol, ((pMsg->DeviceID)&0x10000000) >> 28, (pMsg->DeviceID)&0xfffffff, pMsg->Dir, pMsg->Seq, pMsg->Length, pMsg->OPType);
	for(i=0; i<pMsg->Length; i++)
		u1_printf("%02X ", pMsg->UserBuf[i]);
	u1_printf("\r\n");
	
	
	if(pMsg->OPType == CMD_CONTROL)
	{
		u1_printf("\r\n 收到控制指令\r\n");
		
		if(GetOutputChannelCount() == 0)
		{
			u1_printf("\r\n 没有初始化输出通道,不响应控制指令\r\n");
			return;
		}
		s_RecControlCmdFlag = TRUE;
		g_ArriveReportingTime = TRUE;		//上报标志置位
		SetStopModeFlag(FALSE);	//置位唤醒标志,防止控制指令未执行完就进入停止模式
		Init_Relay_Power();		//开启继电器电源,配置端口
//		Init_Relay_Port();
		delay_ms(100);	
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], pMsg->UserBuf, pMsg->Length);
		Debug_Manual_Ctrl(LORA_COM, sendbuf, sendlenth + pMsg->Length);//回复控制指令
		CheckChannelState();
//		LORA_WORK_MODE();
//		Debug_Report_Status(LORA_COM);
	}
	
	if(pMsg->OPType == CMD_REPORT_D || pMsg->OPType == CMD_REPORT_S)
	{
		s_RecDataPackFlag = TRUE;
		g_RecAck = TRUE;
		RTC_TimeStructure.RTC_Hours   = pMsg->UserBuf[0];
		RTC_TimeStructure.RTC_Minutes = pMsg->UserBuf[1];
		RTC_TimeStructure.RTC_Seconds = pMsg->UserBuf[2];
		s_RegistID = pMsg->UserBuf[3];			//上报时间节点
		s_Data_Interval = pMsg->UserBuf[4];		//上报间隔

		if(s_Data_Interval < 2)
		{
			u1_printf("s_Data_Interval = %d, 最低上报间隔为2min...\r\n", s_Data_Interval);
			s_Data_Interval = 2;			
		}
		else if(s_Data_Interval > 120)
		{
			u1_printf("s_Data_Interval = %d, 最高上报间隔为120min...\r\n", s_Data_Interval);
			s_Data_Interval = 120;
		}
		
		if((pMsg->UserBuf[0] == 255) || (pMsg->UserBuf[1] == 255) || (pMsg->UserBuf[2] == 255))
		{
			u1_printf(" 收到应答,网关未在线,不同步时间...\r\n");
			SetLogErrCode(LOG_CODE_GATEWAYNOLINK);
			StoreOperationalData();
		}
		else 
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
			{			
				RTC_TimeShow();
			}
			else
			{
				u1_printf("同步时间失败\r\n");
			}			
		}
		u1_printf("\r\n Time Point: %d,Data_Interval: %dmin ", s_RegistID, s_Data_Interval);
		return;
	}
	
	if(pMsg->OPType == CMD_GET_CONFIG)//获取设备配置
	{
		SetReCMDFlag(FALSE);
		u1_printf(" 获取设备配置..\r\n");
		LORA_WORK_MODE();
		LORA_WORK_DELAY();
		System_get_config(LORA_COM, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		LORA_SLEEP_DELAY();
		LORA_LOWPOWER_MODE();		
		return;
	}
	
	if(pMsg->OPType == CMD_TEST)
	{
		SetReCMDFlag(FALSE);
		LORA_WORK_MODE();
		LORA_WORK_DELAY();
		Lora_TEST_ACK(CMD_TEST);
		LORA_SLEEP_DELAY();
		LORA_LOWPOWER_MODE();		
	}

	if(pMsg->OPType == CMD_REBOOT)
	{
		SetReCMDFlag(FALSE);
		LORA_WORK_MODE();
		delay_ms(200);
		Lora_TEST_ACK(CMD_REBOOT);
		delay_ms(200);
		u1_printf(" 收到重启指令,设备即将重启....\r\n");
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();	
	}
	
	if(pMsg->OPType == CMD_SET_CONFIG) //设置设备配置
	{
		SetReCMDFlag(FALSE);
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		LORA_WORK_MODE();
		delay_ms(10);
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], pMsg->UserBuf, pMsg->Length);
		ReturnVal = System_set_config(LORA_COM, sendbuf, pMsg->Length + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf(" 更新设备参数,等待设备重启\r\n");
		delay_ms(500);
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
	
}
static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(unsigned char *Data, unsigned short Length, TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[MAX_DATA_BUF];
	uint16   PackLengthgth = 0, i;
	
	memset(PackBuff, sizeof(PackBuff), 0);
	
	UnPackMsg(Data+1, Length-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM2(%d):", PackLengthgth);
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
//协议栈0 应用报文接收观察者处理任务
static void TaskForTCLoraRxObser(void)
{
	static TC_COMM_MSG s_RxFrame;
	static unsigned char PackBuff[MAX_DATA_BUF];
	u16 i, PackStartCount = 0, PackEndCount = 0, RecLength = 0;
	static u8 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10], s_RecStartFlag = FALSE;
	static u16 s_LastTime = 0, RecPackNum = 0;
	static u8 s_BuffStartNum = 0, s_BuffEndNum = 0, s_FullPackFlag = FALSE;
	u8 DataPackCount = 0;
	
	if(g_Uart2RxFlag == TRUE && g_USART2_RX_CNT > 1)
	{	
		s_BuffStartNum = PACK_INIT_NUM;
		s_BuffEndNum = 0;
		s_FullPackFlag = FALSE;
		
		if(g_USART2_RX_CNT < MAX_DATA_BUF && g_USART2_RX_CNT > 10)
		{
			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				if(g_USART2_RX_BUF[i] == BOF_VAL)
				{
					s_BuffStartNum = i;					
				}
				else if(g_USART2_RX_BUF[i] == EOF_VAL)
				{
					s_BuffEndNum = i;
				}
				
				if((s_BuffStartNum != PACK_INIT_NUM) && (s_BuffEndNum - s_BuffStartNum >= 14) && (s_BuffEndNum > 0))
				{
					DataPackCount = (s_BuffEndNum - s_BuffStartNum + 1);
					memcpy(&PackBuff[s_BuffStartNum], &g_USART2_RX_BUF[s_BuffStartNum], DataPackCount);
					RecPackNum += DataPackCount;
					s_FullPackFlag = TRUE;
					break;
				}				
			}
			
			if(s_FullPackFlag == FALSE)
			{
				memcpy(&PackBuff[RecPackNum], g_USART2_RX_BUF, g_USART2_RX_CNT);	
					
				RecPackNum += g_USART2_RX_CNT;				
			}
			
			if(RecPackNum > MAX_DATA_BUF)
			{
				u1_printf(" Buf Over\r\n");
				s_RecStartFlag = FALSE; 
				RecPackNum = 0;
				memset(PackBuff, 0, sizeof(PackBuff));
				Clear_Uart2Buff();
				return;
			}
			
			s_RecStartFlag = TRUE;	
			s_LastTime = GetSystem10msCount();
		}
		else
		{
			u1_printf(" Data Count Over\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			Clear_Uart2Buff();
			return;
		}
		Clear_Uart2Buff();
	}
	
	if(PackBuff[0] == BOF_VAL && PackBuff[RecPackNum - 1] == EOF_VAL && RecPackNum >= 15)
	{
		//连包处理
		PackStartCount = 0;
		PackEndCount = 0;
		
		for(i=0; i<RecPackNum; i++)
		{
			if(PackBuff[i] == BOF_VAL)	//寻找包头个数
			{
				s_PackStartAddr[PackStartCount] = i;
				PackStartCount++;
			}
			else if(PackBuff[i] == EOF_VAL)	//寻找包尾个数
			{
				s_PackEndAddr[PackEndCount] = i;
				PackEndCount++;
			}
		}
		
		if(PackStartCount == PackEndCount)	//是完整的数据包
		{
			if(PackStartCount > 1)
			{
				u1_printf("\r\n %d Packs\r\n", PackStartCount);
			}
			s_PackCount = PackStartCount;
		}
		else
		{
			u1_printf(" StartCount != EndCount\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			return;
		}
		
		if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
		{
			for(i=0; i<s_PackCount; i++)
			{
				memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));
				
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);
				
				RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
				
				if(RecLength > LORA_BUFF_MAX)
				{
					u1_printf(" Err Lora Length(%d)\r\n", RecLength);
					s_RecStartFlag = FALSE;
					memset(PackBuff, 0, sizeof(PackBuff));
					RecPackNum = 0;
					return;
				}
				
				memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
				
				if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
				{
					OnRecLoraData(USART2, &s_RxFrame);
				}
			}		
		}
		s_RecStartFlag = FALSE;
		memset(PackBuff, 0, sizeof(PackBuff));
		RecPackNum = 0;
	}
	else
	{
			

	}
	
	if(s_RecStartFlag && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 100)) 
	{
		u1_printf("\r\n ------------Lora Rec OverTime(%d)\r\n", RecPackNum);
		for(i=0; i<RecPackNum; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");	
		s_RecStartFlag = FALSE; 
		RecPackNum = 0;
		s_LastTime = GetSystem10msCount();
		memset(PackBuff, 0, sizeof(PackBuff));
		Clear_Uart2Buff();	
	}
}

void TCProtocolForLoraProcess(void)
{
	if(s_TCProtocolForLoraRunFlag)
	{
		TaskForTCLoraRxObser();
	}
}

