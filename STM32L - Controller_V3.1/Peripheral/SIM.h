#ifndef  _SIM_H_
#define	 _SIM_H_

#define		SIM800C_ENABLE_TYPE		GPIOA		//δʹ��
#define		SIM800C_ENABLE_PIN		GPIO_Pin_8

#define		SIM_PWR_ON()			GPIO_SetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);
#define		SIM_PWR_OFF()			GPIO_ResetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);

void SIM800CPortInit(void);

void SIM800CProcess(void);

void SIM_Send_Alive(void);

unsigned int SIM_Report_Data(void);

#endif
