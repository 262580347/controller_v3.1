#include "main.h"
#include "SIM.h"

#define COM_DATA_SIZE	256

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通


void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
			
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN | LORA_M1_PIN | LORA_AUX_PIN;//LORA M0 M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
}

void SIM800CProcess(void)
{
	u8 i;
	u16 nTemp = 0;
	static u16 s_LastTime = 0, s_OverCount = 0, s_LastDataTime = 0, s_PowerTime = 0;
	static unsigned int s_RTCSumSecond = 0;
	static u8 s_ATAckCount = 0, s_APN = 0;
	static u8 s_APNFlag = 1, s_State = 1, s_Step = 1, s_First = FALSE, s_ErrCount = 0, s_RetryCount = 0;
	char * p1 = NULL, buf[10], str[100];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		s_LastTime = GetSystem10msCount();
	}
			
	switch(s_State)
	{	
		case 0:
			s_RetryCount++;
			u1_printf("\r\n 重试次数:%d\r\n", s_RetryCount);
			s_State++;
			if(s_RetryCount >= 10)
			{
				u1_printf("超时未连上GPRS\r\n");
				SetLogErrCode(LOG_CODE_TIMEOUT);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
				break;
			}
		break;
			
		case 1:   //模块断电
			SetTCProtocolRunFlag(FALSE);
			SIM800CPortInit();
			SIM_PWR_OFF();
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("[SIM800C]模块断电,延时6S.\r\n");
		break;
		 
		case 2:	//断电后延时5S上电,模块上电自动开机
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 600 )
			{
				u1_printf("[SIM800C]模块上电,延时14S.\r\n");
				SIM_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 3: 	//上电完成后,初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1400 )
			{
				u1_printf("[SIM800C]模块上电完成,检测模块波特率.\r\n");
				u2_printf("AT\r");
				delay_ms(50);
				u2_printf("AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 5 )
			{
				Clear_Uart2Buff();
				u2_printf("AT+CIURC=0\r");		//发送"AT\r"，查询模块是否连接，返回"OK"
				u1_printf(" [SIM]AT+CIURC=0\r\n");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
					u1_printf(" Close Call Ready\r\n");
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 15 )
			{				
					u1_printf(" [SIM]OK无响应\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 4 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						StoreOperationalData();
						s_ErrCount = 0;
						s_State = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(s_Step)
				{
					case 1:
						u2_printf("ATE0\r");
						u1_printf("[SIM]->ATE0\r\n");
					break;
					case 2:
						u2_printf("AT+CPIN?\r");
					break;
					case 3:
						u2_printf("AT+CCID\r");
					break;
					case 4:
						u2_printf("AT+CIMI\r");
					break;
					case 5:
						u2_printf("AT+CGSN\r");
					break;
					case 6:
						u2_printf("AT+CSQ\r");	
					break;
					case 7:
						u2_printf("AT+COPS?\r");
					break;	
					case 8:
						u2_printf("AT+CGATT=1\r");	
					break;				
					case 9:
						u2_printf("AT+CGREG?\r");
					break;		
					case 10:
						u2_printf("AT+CREG?\r");
					break;					
					case 11:
						u2_printf("AT+CGATT?\r");
					break;				
					case 12:
						u2_printf("AT+CGATT=1\r");				
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}		
		break;
		
		case 7:

			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();						
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
					//	u1_printf("检测SIM卡： ");
						if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
						{					
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
							u1_printf("\r\n  SIM Card OK\r\n");
						}
						else
						{
							s_State--;	
							s_ErrCount++;
							
							if(s_ErrCount >= 5)
							{
								SetLogErrCode(LOG_CODE_NO_CARD);
								u1_printf(" No Card!\r\n");
								s_ErrCount = 0;
								s_State = 1;
								s_Step = 1;								
							}
						}						
					break;	
						
					case 3:				//ICCID查询
						//中国移动:898600；898602；898604；898607 
						//中国联通:898601、898606、898609
						//中国电信:898603、898611。
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							if((g_USART2_RX_BUF[2]=='8')&&(g_USART2_RX_BUF[3]=='9')&&(g_USART2_RX_BUF[4]=='8')&&(g_USART2_RX_BUF[5]=='6'))
							{
								i = (g_USART2_RX_BUF[6] - '0') * 10 + (g_USART2_RX_BUF[7] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
										StoreOperationalData();
										u1_printf("CCID: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
										StoreOperationalData();
										u1_printf("CCID: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;
										StoreOperationalData();
										u1_printf("CCID: NO APN INFO\r\n");	
										break;
								}
							}
										
						//	u1_printf("制造商：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:			//IMSI查询
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							if(!s_APNFlag &&(g_USART2_RX_BUF[2]=='4')&&(g_USART2_RX_BUF[3]=='6')&&(g_USART2_RX_BUF[4]=='0'))
							{
								i = (g_USART2_RX_BUF[5] - '0') * 10 + (g_USART2_RX_BUF[6] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
										u1_printf("IMSI: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
										u1_printf("IMSI: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;	
										u1_printf("IMSI: NO APN INFO\r\n");	
										break;
								}
							}
								
						//	u1_printf("模块名称：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("序列号：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 6:	
					//	u1_printf("检测信号质量： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
						{							
							//u1_printf("%s\r\n", g_USART2_RX_BUF);							
							strncpy(buf,p1+6,2);
							nTemp = atoi( buf);
							if( nTemp < 10 )								
							{
								s_State--;	
								s_ErrCount++;
								SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								
								u1_printf(" Weak Signal\r\n");
							}
							else
							{
								s_State--;	
								s_Step++;	
								s_ErrCount = 0;
								ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
								u1_printf("\r\n Signal OK\r\n");
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 7:
						//u1_printf(" 运营商名称： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
						{				
							if(!s_APNFlag)
							{
								if((strstr((const char *)g_USART2_RX_BUF,"CHINA MOBILE")) != NULL )  
								{
									s_APN = APN_CMNET;
									s_APNFlag = 1;
									u1_printf("COPS: CHINA MOBILE\r\n");
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								{
									s_APN = APN_UNINET;
									s_APNFlag = 1;
									u1_printf("COPS: UNICOM\r\n");
								}
								else
								{
									s_APNFlag = 0;
									u1_printf("COPS: UNRECOGNIZABLE\r\n");
								}
							}
	
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
						//	u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}
					break;		
						
					case 8:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							u1_printf(" Set GPRS Attach OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
						}
						else
						{
							s_State--;	
							s_ErrCount++;
							if(s_ErrCount == 30)
							{		
								u1_printf(" GPRS Attach Fail,Check SIM Card\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						
					break;
										
					case 9:
						//u1_printf(" 模块注册网络: ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+10));
							switch( nTemp )								
							{
								case 0:
								u1_printf("\r\n [SIM]Module registration - not registered\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("\r\n [SIM]Module registration network - successful\r\n");
								s_State--;
								s_Step++;
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("\r\n [SIM]Registration in progress,Count=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM]Registration has been rejected,Count=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("\r\n [SIM]The registration status is not recognized\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GPRS注册网络失败，查询GSM\r\n");
								SetLogErrCode(LOG_CODE_UNATTACH);
								s_ErrCount = 0;								
								s_Step++;
							}
							
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
						
					case 10:
						//u1_printf(" 模块注册网络: ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+9));
							switch( nTemp )								
							{
								case 0:
								u1_printf("\r\n [SIM]Module registration - not registered\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("\r\n [SIM]Module registration network - successful\r\n");
								s_State--;
								s_Step++;
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("\r\n [SIM]Registration in progress,Count=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM]Registration has been rejected,Count=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("\r\n [SIM]The registration status is not recognized\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GSM registration network failed. Check attachment status\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
					
					case 11:	
						//u1_printf("GPRS附着状态: ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
						{					
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+8));
							if( nTemp == 0 )								
							{
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount == 30)
								{									
									s_Step++;
									s_ErrCount++;
								}
							}
							else
							{
								s_State++;	
								s_ATAckCount = 0;
								s_Step = 1;	
								s_ErrCount = 0;
							}
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 12:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step--;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
				}
				
				if(s_ErrCount > 100 )
				{
					SetLogErrCode(LOG_CODE_UNATTACH);
					StoreOperationalData();
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
				
				Clear_Uart2Buff();
				s_LastTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					StoreOperationalData();
					u1_printf(" AT No Ack\r\n");
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf("AT Timeout\r\n");
				}
				
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
		
		case 8:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				switch(s_Step)
				{
						case 1:
							u2_printf("AT+CIPCLOSE=1\r");
							u1_printf(" [SIM]->Close current link\r\n");
					
						break;
						case 2:
							u2_printf("AT+CIPSHUT\r");
							u1_printf(" [SIM]->Close all links\r\n");
						break;
						case 3:
							u2_printf("AT+CIPMODE=1\r");
							u1_printf(" [SIM]->Set to pass-through mode\r\n");
						break;
						case 4:
							if(s_APNFlag)
							{
								switch(s_APN)
								{
									case APN_CMNET:
										u2_printf("AT+CSTT=\"CMNET\"\r");

										break;
									case APN_UNINET:
										u2_printf("AT+CSTT=\"UNINET\"\r");
										break;
									default:
										u2_printf("AT+CSTT?\r");
										break;
								}
							}	
							else 
							{								
								u2_printf("AT+CSTT?\r");
								u1_printf("[SIM]->Undefine\r\n");	
							}
						break;
						case 5:
							u2_printf("AT+CIICR\r");
							u1_printf(" [SIM]->Establish wireless link GPRS\r\n");
						break;
						case 6:
							u2_printf("AT+CIFSR\r");
							u1_printf(" [SIM]->Get the local IP address\r\n\r\n");
						break;
						
						case 7:
						break;
						
						default:
							s_State = 1;
							s_Step = 1;
						break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		
		break;
	
		case 9:
			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"+CIPCLOSE")) != NULL )
						{
							s_Step++;	
							s_State--;		
													
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{
							s_Step++;	
							s_State--;									
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"SHUT OK")) != NULL )
						{
						//	u1_printf("关闭移动场景\r\n");
							s_Step++;	
							s_State--;															
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("设置为透传模式\r\n");
							s_Step++;	
							s_State--;														
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("APN OK\r\n");
							s_Step++;	
							s_State--;															
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
					
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("激活移动场景 OK\r\n");
							s_Step++;	
							s_State--;														
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"+PDP: DEACT")) != NULL )
						{
							s_State--;	
							s_Step = 1;
							s_ErrCount++;
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
					case 6:
						//u1_printf("模块本地IP:");
						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
						{					
							s_Step = 1;	
							s_State++;		
							s_ATAckCount = 0;
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}						
					break;
					
					case 7:	
						u1_printf("IP Head:=%s\r\n", g_USART2_RX_BUF);
						s_State++;
						s_ATAckCount = 0;
						s_Step = 1;
					break;
						
				}
				
				if(s_ErrCount > 50 )
				{
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 3)
				{
					s_ATAckCount = 0;
					u1_printf(" AT No Ack\r\n");
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf("AT Timeout\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 10:
			u1_printf(" [SIM]Establishing TCP links\r\n");
			sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			Clear_Uart2Buff();
			u2_printf(str);	
			u1_printf(str);	//串口输出AT命令
			u1_printf("\n");
			s_LastTime = GetSystem10msCount();
			s_State++;			
		break;
		
		case 11:
			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
					
				if((strstr((const char *)g_USART2_RX_BUF,"ALREADY CONNECT")) != NULL )
				{
					s_Step = 1;	
					s_State = 8;		
					SetTCModuleReadyFlag(FALSE);	
					u1_printf(" GPRS link established\r\n");				
				}				
				else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
				{		
					s_ErrCount++;
					s_State--;	
					s_State = 8;
					s_Step = 1;					
				}				
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT OK")) != NULL )
				{
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(1);		//连接成功========================
					u1_printf(" GPRS link successful\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL")) != NULL )
				{		
					s_ErrCount++;
					s_State--;
					u1_printf(" GPRS link failed\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT")) != NULL )
				{
					ClearLogErrCode(LOG_CODE_UNATTACH);
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(1);		//连接成功========================
					u1_printf(" GPRS link successful\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
				{		
					u1_printf("\r\n Wait Connect...\r\n");	
					s_LastTime = GetSystem10msCount();
				}
				else
				{
					s_State--;	
					s_ErrCount++;
					u1_printf(" Unrecognized instruction\r\n");
				}
					
				
				if(s_ErrCount > 20 )
				{
					s_ErrCount = 0;
					s_State = 1;
					s_Step = 1;
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_State--;
				u1_printf(" AT No Ack\r\n");
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
				if(s_ErrCount >= 3)
				{
					SetLogErrCode(LOG_CODE_TIMEOUT);
					StoreOperationalData();
					s_State = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}			
		break;
		
		case 12:
			s_State = 50;
			SetTCProtocolRunFlag(TRUE);
			s_LastTime = GetSystem10msCount();
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪s_LastDataTime
			{
				Debug_Report_Status(SIM_COM);	//发送数据包
				delay_ms(50);
				Mod_Send_Alive(SIM_COM);	//发送心跳包
				
				if( CalculateTime(GetSystem10msCount(),s_LastDataTime) >= p_sys->Data_interval*100 )
				{
					s_LastDataTime = GetSystem10msCount();
					Mod_Report_Data(SIM_COM);	
				}
				
				s_State++;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				s_State = 0;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(g_TCRecStatus.AliveAckFlag || g_TCRecStatus.StatusAckFlag)
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.StatusAckFlag = FALSE;
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2500 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n 超时未收到回复包  重发\r\n");
				
				Clear_Uart2Buff();
				s_OverCount++;
				if(s_OverCount >= 5)
				{
					s_OverCount = 0;
					s_State = 0;
					s_Step = 1;
					s_ErrCount = 0;
					SetTCModuleReadyFlag(TRUE);
					SetOnline(FALSE);
				}
			}
			
		break;
					
		case 52:
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = GetSystem10msCount();
		break;
		
		case 53:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Heart_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				s_RetryCount = 0;

				s_State = 50;
			}		

			if( CalculateTime(GetSystem10msCount(),s_PowerTime) >= 6000 )
			{
				s_PowerTime = GetSystem10msCount();
				StartGetBatVol();			//电池电压检测
			}
		break;
			
		case  54:
			if(g_PowerDetectFlag)	//SIM重新上电初始化
			{
				s_State = 0;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
				SetTCModuleReadyFlag(0);
			}
		break;		
	}
	
}


