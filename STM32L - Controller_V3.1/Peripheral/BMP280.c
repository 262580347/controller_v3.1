/**********************************
说明：BMP280大气压力传感器驱动
	  注意SDO接高电平，IIC地址为0x76
	  SDO接低电平，IIC地址为0x77
	  若接口更改则需修改宏定义及IIC_Init中的GPIO
	  IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3C);	//睡眠模式  用于低功耗设备
	  IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);	//正常模式  用于传感器
作者：关宇晟
版本：V2017.12.26
***********************************/

#include "main.h"
#include "BMP280.h"
#include <math.h>

#define __STAND_TEMP      15.0f    // 15 C
#define __STAND_PRES      1013.25f // 101.325kPa = 1013.25mbar

#define  GPIO_I2C_DIR_SEND         ((unsigned char)0x00)
#define  GPIO_I2C_DIR_RECV         ((unsigned char)0x01)

#define BMP280_I2C_ADDR                         0x76 // SDO 接地 0x76，SDO接VCC  0x77

#define BMP280_REG_TEMP_XLSB                    0xFC
#define BMP280_REG_TEMP_LSB                     0xFB
#define BMP280_REG_TEMP_MSB                     0xFA
#define BMP280_REG_PRESS_XLSB                   0xF9
#define BMP280_REG_PRESS_LSB                    0xF8
#define BMP280_REG_PRESS_MSB                    0xF7
#define BMP280_REG_CONFIG                       0xF5
#define BMP280_REG_CTRL_MEAS                    0xF4
#define BMP280_REG_STATUS                       0xF3
#define BMP280_REG_RESET                        0xE0
#define BMP280_REG_ID                           0xD0

#define BMP280_REG_DIG_T1                       0x88
#define BMP280_REG_DIG_T2                       0x8A
#define BMP280_REG_DIG_T3                       0x8C

#define BMP280_REG_DIG_P1                       0x8E
#define BMP280_REG_DIG_P2                       0x90
#define BMP280_REG_DIG_P3                       0x92
#define BMP280_REG_DIG_P4                       0x94
#define BMP280_REG_DIG_P5                       0x96
#define BMP280_REG_DIG_P6                       0x98
#define BMP280_REG_DIG_P7                       0x9A
#define BMP280_REG_DIG_P8                       0x9C
#define BMP280_REG_DIG_P9                       0x9E
      
#define BMP280_ID                               0x58	//0x56 0x57 0x58?BMP280
#define BMP280_RESET                            0xB6


#define 	BMP_DETECT_INTERVAL		10

#define		BMP_FORCE_MODE		0x25
#define		BMP_WORK_MODE		0x27
#define		BMP_SLEEP_MODE		0x24

uint16 dig_T1;
int16  dig_T2;
int16  dig_T3;

uint16 dig_P1;
int16  dig_P2;
int16  dig_P3;
int16  dig_P4;
int16  dig_P5;
int16  dig_P6;
int16  dig_P7;
int16  dig_P8;
int16  dig_P9;

int32  t_fine;


float g_fBMPTemp = 0.0;
float g_fBMPPress = 0.0;
float g_fBMPAltitude = 0.0;

unsigned char g_BMPGetFlag = FALSE;

float GetBMPTemp( void )
{
	return g_fBMPTemp;
}

float GetBMPPress( void )
{
	return g_fBMPPress;
}

float GetBMPAltitude( void )
{
	return g_fBMPAltitude;
}


#define ACK 	1
#define NOACK	0

#define	SDA_PIN		GPIO_Pin_9
#define	SDA_TYPE	GPIOB
#define	SCK_PIN		GPIO_Pin_8
#define	SCK_TYPE	GPIOB

#define SDA_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}
#define SDA_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}


#define BMP_SCK_HIGH    GPIO_SetBits(SCK_TYPE, SCK_PIN)
#define BMP_SCK_LOW 	GPIO_ResetBits(SCK_TYPE, SCK_PIN)
#define BMP_SDA_HIGH    GPIO_SetBits(SDA_TYPE, SDA_PIN)
#define BMP_SDA_LOW 	GPIO_ResetBits(SDA_TYPE, SDA_PIN)
#define READ_BMP_SDA   	GPIO_ReadInputDataBit(SDA_TYPE, SDA_PIN)

#define	BMP_DELAY	delay_us(5)

static void IIC_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_SetBits(SDA_TYPE, SDA_PIN);
	GPIO_SetBits(SCK_TYPE, SCK_PIN);    		
}

static void IIC_Start(void)
{
	SDA_OUT();
	BMP_SDA_HIGH;
	BMP_SCK_HIGH;
	BMP_DELAY;
	BMP_SDA_LOW;
	BMP_DELAY;
	BMP_SCK_LOW;
}


static void IIC_Stop(void)
{
	SDA_OUT();
	BMP_SCK_LOW;
	BMP_SDA_LOW;
	BMP_DELAY;
	BMP_SCK_HIGH;
	BMP_DELAY;
	BMP_SDA_HIGH;
	BMP_DELAY;
}

static void IIC_Write_ACK(void)
{
	BMP_SCK_LOW;
	SDA_OUT();
	BMP_SDA_LOW;
	BMP_DELAY;
	BMP_SCK_HIGH;
	BMP_DELAY;
	BMP_SCK_LOW;
}

static void IIC_Write_No_ACK(void)
{
	BMP_SCK_LOW;	
	SDA_OUT();
	BMP_SDA_HIGH;
	BMP_DELAY;
	BMP_SCK_HIGH;
	BMP_DELAY;
	BMP_SCK_LOW;
}

static u8 IIC_Wait_ACK(void)
{
	u8 ucErrTime=0;
	SDA_IN();
	BMP_SDA_HIGH;	
	BMP_DELAY;	
	BMP_SCK_HIGH;
	BMP_DELAY;
	while(READ_BMP_SDA)
	{
		ucErrTime++;
		if(ucErrTime >250)
		{
			IIC_Stop();
			return 1;
		}
	}
	BMP_SCK_LOW;
	return 0;
}

static void IIC_WriteByte(u8 data)
{
	u8 i;
	
	SDA_OUT();
	BMP_SCK_LOW;
	for(i=0; i<8; i++)
	{
		if(data & 0x80)
			BMP_SDA_HIGH;
		else
			BMP_SDA_LOW;
		data <<= 1;
		BMP_DELAY;
		BMP_SCK_HIGH;
		BMP_DELAY;
		BMP_SCK_LOW;
		BMP_DELAY;
	}	
}

static u8 IIC_ReadByte(u8 IsAck)
{
	u8 i, receive = 0;
	SDA_IN();
	for(i=0; i<8; i++)
	{
		BMP_SCK_LOW;
		BMP_DELAY;
		BMP_SCK_HIGH;
		BMP_DELAY;
		receive <<= 1;
		if(READ_BMP_SDA)
		{
			//receive++;
			receive |= 1;
		}
		BMP_DELAY;
	}
	if(IsAck == ACK )
		IIC_Write_ACK();
	else
		IIC_Write_No_ACK();
	
	return receive;
}	


static u8  IIC_Read_Data (unsigned char addr, unsigned char reg, unsigned char *pbuf, int len)
{
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	IIC_Wait_ACK();
    
    IIC_WriteByte(reg);
    IIC_Wait_ACK();
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x01);
    IIC_Wait_ACK();
    while (len) 
	{ 
		if (len == 1) 
		{
			*pbuf = IIC_ReadByte(NOACK);
		} 
		else 
		{
			*pbuf = IIC_ReadByte(ACK);
		}
        pbuf++;
        len--;
    }
    IIC_Stop();
    
    return 0;
}

static u8  IIC_Write_Reg (unsigned char addr, unsigned char reg, unsigned char data)
{
	u8 err = 1;
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	err = IIC_Wait_ACK();   
	if(err)
		return 1;
    
    IIC_WriteByte(reg);
	err = IIC_Wait_ACK();   
	if(err)
		return 1;
	IIC_WriteByte(data);

	err = IIC_Wait_ACK();   
	if(err)
		return 1;
    IIC_Stop();
    
    return 0;
}

static u8  IIC_Read_Reg (unsigned char addr, unsigned char reg, /* @out */unsigned char *data)
{
	u8 err = 1;
	
    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x00);
	err = IIC_Wait_ACK();   
	if(err)
		return 1;
    IIC_WriteByte(reg);
	err = IIC_Wait_ACK();   
	if(err)
		return 1;

    IIC_Start();
    IIC_WriteByte((addr << 1) | 0x01);
	err = IIC_Wait_ACK();   
	if(err)
		return 1;
	
    *data = IIC_ReadByte(NOACK);
    IIC_Stop();

    return 0;
}


int BMP280_Init (void)
{
    unsigned char uc_ID = 0, err = 0;

    IIC_Init();
        
    err = IIC_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &uc_ID);
    
	if(err)
	{
		printf("\r\n [BMP280] 初始化失败\r\n");
		g_BMPGetFlag = FALSE;
		return ERROR_VALUE;
	}
	
    if (uc_ID != BMP280_ID) 
	{
        printf("\r\n [BMP280] uc_ID = 0x%02x，not BMP280 ID!Init failed!\r\n", uc_ID);
		//printf("[BMP280] Init failed!\n");
        return ERROR_VALUE;
    }
   
    printf("\r\n [BMP280] ID = 0x%02x, Init Succeed ...\n", uc_ID);

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T1, (unsigned char *)&dig_T1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T2, (unsigned char *)&dig_T2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T3, (unsigned char *)&dig_T3, 2);

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P1, (unsigned char *)&dig_P1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P2, (unsigned char *)&dig_P2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P3, (unsigned char *)&dig_P3, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P4, (unsigned char *)&dig_P4, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P5, (unsigned char *)&dig_P5, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P6, (unsigned char *)&dig_P6, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P7, (unsigned char *)&dig_P7, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P8, (unsigned char *)&dig_P8, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P9, (unsigned char *)&dig_P9, 2);
    
 //   IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);
//	IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
    return 0;
}

static int BMP280Get_Temp_Raw (void)
{
    unsigned char temp[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_TEMP_MSB, temp, 3);
    
    result = (temp[0] << 16) | (temp[1] << 8) | (temp[2]);
    
    result >>= 4;
    
    return result;
}

static int BMP280Get_Press_Raw (void)
{
    unsigned char press[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_PRESS_MSB, press, 3);
    
    result = (press[0] << 16) | (press[1] << 8) | (press[2]);
    
    result >>= 4;
    
    return result;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of "5123" equals 51.23 DegC.
// t_fine carries fine temperature as global value

static int BMP280Get_Temp_x100 (void)
{
	
	//int32  t_fine;
    int raw_temp;   
    int adc_T;
    int32 var1, var2, T;
	
	raw_temp = BMP280Get_Temp_Raw();
	adc_T = raw_temp;
    
    var1 = ((((adc_T>>3) - ((int32)dig_T1<<1))) * ((int32)dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((int32)dig_T1)) * ((adc_T>>4) - ((int32)dig_T1))) >> 12) * ((int32)dig_T3)) >> 14;
    
    t_fine = var1 + var2;
    
    T = (t_fine * 5 + 128) >> 8;
    
    return T;
}

static float BMP280Get_Temp (void)
{
    int tempc_x100 = BMP280Get_Temp_x100();
    return (float)(tempc_x100 / 100.0f);
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of "24674867" represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static unsigned int BMP280Get_Press_Q24_8 (void)
{
    int raw_press = BMP280Get_Press_Raw();
    
    int adc_P = raw_press;
    
    long long var1, var2, p;
    
    var1 = ((long long)t_fine) - 128000;
    var2 = var1 * var1 * (long long)dig_P6;
    var2 = var2 + ((var1*(long long)dig_P5)<<17);
    var2 = var2 + (((long long)dig_P4)<<35);
    var1 = ((var1 * var1 * (long long)dig_P3)>>8) + ((var1 * (long long)dig_P2)<<12);
    var1 = (((((long long)1)<<47)+var1))*((long long)dig_P1)>>33;
    
    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    
    p = 1048576- adc_P;
    p = (((p<<31)-var2)*3125)/var1;
    var1 = (((long long)dig_P9) * (p>>13) * (p>>13)) >> 25;
    var2 = (((long long)dig_P8) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + (((long long)dig_P7)<<4);
    
    return (unsigned int)p;
}

static float BMP280Get_Press (void)
{
    float pa;
    unsigned int press_q24_8 = BMP280Get_Press_Q24_8();
    pa = (float)(press_q24_8 / 256.0f);
    
    return pa / 100.0f;
}

// h = Z2-Z1 = 18400(1+atm)LgP1/P2
static float BMP280Get_Altitude (float temperature, float pressure)
{
    return 18400.0f * (1.0f + temperature / 273.0f) * log10(__STAND_PRES / pressure);
}


unsigned char BMP_Process( unsigned short nMain10ms )
{
	float fBMPPress = 0.0, fBMPTemp;
	u8 ack = 0;
	static u8 s_State = 1;
	static u16 s_LastTime;
	
	if(g_BMPGetFlag == FALSE)
	{
		switch(s_State)
		{
			case 1:
				s_State++;
				s_LastTime = nMain10ms;
				IIC_Init();
				//IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);
				IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_FORCE_MODE);
			break;
			
			case 2:
				if(CalculateTime(nMain10ms, s_LastTime) >= 4)
				{					
					fBMPTemp = BMP280Get_Temp();
					fBMPPress = BMP280Get_Press();
					BMP280Get_Altitude(fBMPTemp, fBMPPress);
					if(fBMPPress == 0)
					{
						g_fBMPPress = ERROR_VALUE;
						printf("\r\n 错误的BMP数据\r\n");
						
					}
					else
					{
						g_fBMPPress = fBMPPress;
					//	printf("\r\n Pressure:%2.2fhPa\r\n", g_fBMPPress);
					}
					s_LastTime = nMain10ms;
					ack = IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						g_BMPGetFlag = TRUE;
					}
					else
					{
						s_State++;
						printf("no ack\r\n");
					}
				}
			break;
				
			case 3:
				if(CalculateTime(nMain10ms, s_LastTime) >= 4)
				{
					s_LastTime = nMain10ms;
					ack = IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						g_BMPGetFlag = TRUE;
					}
					else
					{
						s_State++;
						printf("no ack\r\n");
					}
				}
			break;
				
			case 4:
				if(CalculateTime(nMain10ms, s_LastTime) >= 2)
				{
					s_LastTime = nMain10ms;
					ack = IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						g_BMPGetFlag = TRUE;
					}
					else
					{
						s_State = 1;
						g_BMPGetFlag = TRUE;
						printf("no ack\r\n");
					}
				}
			break;
			
			
		}
	}
		
	return 0;
	
}



